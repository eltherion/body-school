// Generated by Apple Swift version 3.0.2 (swiftlang-800.0.63 clang-800.0.42.1)
#pragma clang diagnostic push

#if defined(__has_include) && __has_include(<swift/objc-prologue.h>)
# include <swift/objc-prologue.h>
#endif

#pragma clang diagnostic ignored "-Wauto-import"
#include <objc/NSObject.h>
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#if !defined(SWIFT_TYPEDEFS)
# define SWIFT_TYPEDEFS 1
# if defined(__has_include) && __has_include(<uchar.h>)
#  include <uchar.h>
# elif !defined(__cplusplus) || __cplusplus < 201103L
typedef uint_least16_t char16_t;
typedef uint_least32_t char32_t;
# endif
typedef float swift_float2  __attribute__((__ext_vector_type__(2)));
typedef float swift_float3  __attribute__((__ext_vector_type__(3)));
typedef float swift_float4  __attribute__((__ext_vector_type__(4)));
typedef double swift_double2  __attribute__((__ext_vector_type__(2)));
typedef double swift_double3  __attribute__((__ext_vector_type__(3)));
typedef double swift_double4  __attribute__((__ext_vector_type__(4)));
typedef int swift_int2  __attribute__((__ext_vector_type__(2)));
typedef int swift_int3  __attribute__((__ext_vector_type__(3)));
typedef int swift_int4  __attribute__((__ext_vector_type__(4)));
typedef unsigned int swift_uint2  __attribute__((__ext_vector_type__(2)));
typedef unsigned int swift_uint3  __attribute__((__ext_vector_type__(3)));
typedef unsigned int swift_uint4  __attribute__((__ext_vector_type__(4)));
#endif

#if !defined(SWIFT_PASTE)
# define SWIFT_PASTE_HELPER(x, y) x##y
# define SWIFT_PASTE(x, y) SWIFT_PASTE_HELPER(x, y)
#endif
#if !defined(SWIFT_METATYPE)
# define SWIFT_METATYPE(X) Class
#endif
#if !defined(SWIFT_CLASS_PROPERTY)
# if __has_feature(objc_class_property)
#  define SWIFT_CLASS_PROPERTY(...) __VA_ARGS__
# else
#  define SWIFT_CLASS_PROPERTY(...)
# endif
#endif

#if defined(__has_attribute) && __has_attribute(objc_runtime_name)
# define SWIFT_RUNTIME_NAME(X) __attribute__((objc_runtime_name(X)))
#else
# define SWIFT_RUNTIME_NAME(X)
#endif
#if defined(__has_attribute) && __has_attribute(swift_name)
# define SWIFT_COMPILE_NAME(X) __attribute__((swift_name(X)))
#else
# define SWIFT_COMPILE_NAME(X)
#endif
#if defined(__has_attribute) && __has_attribute(objc_method_family)
# define SWIFT_METHOD_FAMILY(X) __attribute__((objc_method_family(X)))
#else
# define SWIFT_METHOD_FAMILY(X)
#endif
#if defined(__has_attribute) && __has_attribute(noescape)
# define SWIFT_NOESCAPE __attribute__((noescape))
#else
# define SWIFT_NOESCAPE
#endif
#if !defined(SWIFT_CLASS_EXTRA)
# define SWIFT_CLASS_EXTRA
#endif
#if !defined(SWIFT_PROTOCOL_EXTRA)
# define SWIFT_PROTOCOL_EXTRA
#endif
#if !defined(SWIFT_ENUM_EXTRA)
# define SWIFT_ENUM_EXTRA
#endif
#if !defined(SWIFT_CLASS)
# if defined(__has_attribute) && __has_attribute(objc_subclassing_restricted)
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# else
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# endif
#endif

#if !defined(SWIFT_PROTOCOL)
# define SWIFT_PROTOCOL(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
# define SWIFT_PROTOCOL_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
#endif

#if !defined(SWIFT_EXTENSION)
# define SWIFT_EXTENSION(M) SWIFT_PASTE(M##_Swift_, __LINE__)
#endif

#if !defined(OBJC_DESIGNATED_INITIALIZER)
# if defined(__has_attribute) && __has_attribute(objc_designated_initializer)
#  define OBJC_DESIGNATED_INITIALIZER __attribute__((objc_designated_initializer))
# else
#  define OBJC_DESIGNATED_INITIALIZER
# endif
#endif
#if !defined(SWIFT_ENUM)
# define SWIFT_ENUM(_type, _name) enum _name : _type _name; enum SWIFT_ENUM_EXTRA _name : _type
# if defined(__has_feature) && __has_feature(generalized_swift_name)
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME) enum _name : _type _name SWIFT_COMPILE_NAME(SWIFT_NAME); enum SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_ENUM_EXTRA _name : _type
# else
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME) SWIFT_ENUM(_type, _name)
# endif
#endif
#if !defined(SWIFT_UNAVAILABLE)
# define SWIFT_UNAVAILABLE __attribute__((unavailable))
#endif
#if defined(__has_feature) && __has_feature(modules)
@import ObjectiveC;
@import CoreGraphics;
@import CoreFoundation;
@import UIKit;
#endif

#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
#pragma clang diagnostic ignored "-Wduplicate-method-arg"
@class UIView;
@class NSProgress;

SWIFT_CLASS("_TtC15ARSLineProgress15ARSLineProgress")
@interface ARSLineProgress : NSObject
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly) BOOL shown;)
+ (BOOL)shown;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly) BOOL statusShown;)
+ (BOOL)statusShown;
/**
  \code
     Will interrupt the current .Infinite loader progress and show success animation instead.

  \endcode*/
+ (void)showSuccess;
/**
  Will interrupt the current .Infinite loader progress and show fail animation instead.
*/
+ (void)showFail;
+ (void)show;
+ (void)showWithPresentCompetionBlock:(void (^ _Nonnull)(void))block;
+ (void)showOnView:(UIView * _Nonnull)view;
+ (void)showOnView:(UIView * _Nonnull)view completionBlock:(void (^ _Nonnull)(void))completionBlock;
/**
  Note: initialValue should be from 0 to 100
*/
+ (void)showWithProgressWithInitialValue:(CGFloat)value;
/**
  Note: initialValue should be from 0 to 100
*/
+ (void)showWithProgressWithInitialValue:(CGFloat)value onView:(UIView * _Nonnull)onView;
/**
  Note: initialValue should be from 0 to 100
*/
+ (void)showWithProgressWithInitialValue:(CGFloat)value completionBlock:(void (^ _Nullable)(void))completionBlock;
/**
  Note: initialValue should be from 0 to 100
*/
+ (void)showWithProgressWithInitialValue:(CGFloat)value onView:(UIView * _Nonnull)onView completionBlock:(void (^ _Nullable)(void))completionBlock;
+ (void)showWithProgressObject:(NSProgress * _Nonnull)progress;
+ (void)showWithProgressObject:(NSProgress * _Nonnull)progress completionBlock:(void (^ _Nullable)(void))completionBlock;
+ (void)showWithProgressObject:(NSProgress * _Nonnull)progress onView:(UIView * _Nonnull)onView;
+ (void)showWithProgressObject:(NSProgress * _Nonnull)progress onView:(UIView * _Nonnull)onView completionBlock:(void (^ _Nullable)(void))completionBlock;
+ (void)updateWithProgress:(CGFloat)value;
+ (void)cancelPorgressWithFailAnimation:(BOOL)showFail;
+ (void)cancelPorgressWithFailAnimation:(BOOL)showFail completionBlock:(void (^ _Nullable)(void))completionBlock;
+ (void)hide;
/**
  <code>completionBlock</code> is going to be called on the main queue
*/
+ (void)hideWithCompletionBlock:(void (^ _Nonnull)(void))block;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


SWIFT_CLASS("_TtC15ARSLineProgress28ARSLineProgressConfiguration")
@interface ARSLineProgressConfiguration : NSObject
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) BOOL showSuccessCheckmark;)
+ (BOOL)showSuccessCheckmark;
+ (void)setShowSuccessCheckmark:(BOOL)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGFloat backgroundViewCornerRadius;)
+ (CGFloat)backgroundViewCornerRadius;
+ (void)setBackgroundViewCornerRadius:(CGFloat)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval backgroundViewPresentAnimationDuration;)
+ (CFTimeInterval)backgroundViewPresentAnimationDuration;
+ (void)setBackgroundViewPresentAnimationDuration:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval backgroundViewDismissAnimationDuration;)
+ (CFTimeInterval)backgroundViewDismissAnimationDuration;
+ (void)setBackgroundViewDismissAnimationDuration:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) UIBlurEffectStyle blurStyle;)
+ (UIBlurEffectStyle)blurStyle;
+ (void)setBlurStyle:(UIBlurEffectStyle)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull circleColorOuter;)
+ (CGColorRef _Nonnull)circleColorOuter;
+ (void)setCircleColorOuter:(CGColorRef _Nonnull)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull circleColorMiddle;)
+ (CGColorRef _Nonnull)circleColorMiddle;
+ (void)setCircleColorMiddle:(CGColorRef _Nonnull)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull circleColorInner;)
+ (CGColorRef _Nonnull)circleColorInner;
+ (void)setCircleColorInner:(CGColorRef _Nonnull)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval circleRotationDurationOuter;)
+ (CFTimeInterval)circleRotationDurationOuter;
+ (void)setCircleRotationDurationOuter:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval circleRotationDurationMiddle;)
+ (CFTimeInterval)circleRotationDurationMiddle;
+ (void)setCircleRotationDurationMiddle:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval circleRotationDurationInner;)
+ (CFTimeInterval)circleRotationDurationInner;
+ (void)setCircleRotationDurationInner:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval checkmarkAnimationDrawDuration;)
+ (CFTimeInterval)checkmarkAnimationDrawDuration;
+ (void)setCheckmarkAnimationDrawDuration:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGFloat checkmarkLineWidth;)
+ (CGFloat)checkmarkLineWidth;
+ (void)setCheckmarkLineWidth:(CGFloat)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull checkmarkColor;)
+ (CGColorRef _Nonnull)checkmarkColor;
+ (void)setCheckmarkColor:(CGColorRef _Nonnull)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval successCircleAnimationDrawDuration;)
+ (CFTimeInterval)successCircleAnimationDrawDuration;
+ (void)setSuccessCircleAnimationDrawDuration:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGFloat successCircleLineWidth;)
+ (CGFloat)successCircleLineWidth;
+ (void)setSuccessCircleLineWidth:(CGFloat)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull successCircleColor;)
+ (CGColorRef _Nonnull)successCircleColor;
+ (void)setSuccessCircleColor:(CGColorRef _Nonnull)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval failCrossAnimationDrawDuration;)
+ (CFTimeInterval)failCrossAnimationDrawDuration;
+ (void)setFailCrossAnimationDrawDuration:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGFloat failCrossLineWidth;)
+ (CGFloat)failCrossLineWidth;
+ (void)setFailCrossLineWidth:(CGFloat)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull failCrossColor;)
+ (CGColorRef _Nonnull)failCrossColor;
+ (void)setFailCrossColor:(CGColorRef _Nonnull)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CFTimeInterval failCircleAnimationDrawDuration;)
+ (CFTimeInterval)failCircleAnimationDrawDuration;
+ (void)setFailCircleAnimationDrawDuration:(CFTimeInterval)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGFloat failCircleLineWidth;)
+ (CGFloat)failCircleLineWidth;
+ (void)setFailCircleLineWidth:(CGFloat)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class) CGColorRef _Nonnull failCircleColor;)
+ (CGColorRef _Nonnull)failCircleColor;
+ (void)setFailCircleColor:(CGColorRef _Nonnull)value;
/**
  Use this function to restore all properties to their default values.
*/
+ (void)restoreDefaults;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


@interface UIColor (SWIFT_EXTENSION(ARSLineProgress))
@end

#pragma clang diagnostic pop
