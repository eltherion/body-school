//
//  WeekViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Pantry
import Timepiece

class WeekViewController: UIViewController, LogoPresenter, RYRCalendarDelegate {

    fileprivate let dateFormatter = DateFormatter()
    fileprivate let calender = Calendar.current
    fileprivate let trainingsService = TrainingsService()
    fileprivate var calendarViewController: CalendarViewController?
    var logoView: UIImageView?
    var selectedAdminDate: Date = Date().beginningOfWeek.beginningOfDay
    var weekSelectorView: RYRCalendar? = RYRCalendar()
    var adminView: Bool = false

    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var scheduleView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Menu", comment: ""),
            style: .plain,
            target: self,
            action: #selector(SSASideMenu.presentMenuViewController))

        if adminView {
            navigationItem.rightBarButtonItems = [
                UIBarButtonItem(
                    barButtonSystemItem: UIBarButtonSystemItem.add,
                    target: self,
                    action: #selector(addTraining)),
                UIBarButtonItem(
                title: NSLocalizedString("Week selection", comment: ""),
                style: .plain,
                target: self,
                action: #selector(showWeekSelector))]
            navigationItem.rightBarButtonItems?.forEach({ (barButtonItem) in
                barButtonItem.tintColor = UIColor.colorWithHexString("5BBC7A")
            })
            self.setupWeekSelector()
        }
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
        setLogoFrame(navigationItem)
        setupLayout()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        reloadData()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
        self.calendarViewController?.collectionViewCalendarLayout
            .invalidateLayoutCache()
        self.calendarViewController?.collectionView?.reloadData()
    }

    fileprivate func setupWeekSelector() {
        weekSelectorView?.backgroundColor = UIColor.white
        weekSelectorView?.isHidden = true
        weekSelectorView?.delegate = self
        weekSelectorView?.selectionType = .single
        weekSelectorView?.totalMonthsFromNow = 12*100
        weekSelectorView?.update()
        view.addSubview(weekSelectorView!)
        let _ = weekSelectorView?.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)
        }
        self.setupWeekLabelsForDate(selectedAdminDate)
    }

    fileprivate func setupWeekLabelsForDate(_ date: Date) {
        if let yearView = self.yearView,
            let monthView = self.monthView,
            let weekView = self.weekView,
            let yearLabel: UILabel = yearView.viewWithTag(11) as? UILabel,
            let yearNumberLabel: UILabel = yearView.viewWithTag(21) as? UILabel,
            let monthLabel: UILabel = monthView.viewWithTag(12) as? UILabel,
            let monthNumberLabel: UILabel = monthView.viewWithTag(22) as? UILabel,
            let weekLabel: UILabel = weekView.viewWithTag(13) as? UILabel,
            let weekNumberLabel: UILabel = weekView.viewWithTag(23) as? UILabel,
            let decreaseWeekButton: UIButton = weekView.viewWithTag(3) as? UIButton {

            yearLabel.text = NSLocalizedString("Year", comment: "")
            yearNumberLabel.text = String(date.year)
            monthLabel.text = NSLocalizedString("Month", comment: "")
            monthNumberLabel.text = String(date.month)
            weekLabel.text = NSLocalizedString("Week", comment: "")
            weekNumberLabel.text = String((calender as NSCalendar).component(.weekOfMonth,
                from: selectedAdminDate))
            
            let currentWeekMonday = Date().beginningOfWeek.beginningOfDay
            decreaseWeekButton.isHidden = !adminView && date <= currentWeekMonday
        }
    }

    fileprivate func setupLayout() {
        self.view.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        self.setupWeekLabelsForDate(self.selectedAdminDate)
        if !adminView {
            if let user: User = Pantry.unpack("lastUser"),
                let firstName = user.firstName,
                let lastName = user.lastName {
                self.title = firstName + " " + lastName
            }

            yearView.subviews.forEach({ (subview) in
                subview.isHidden = subview.tag == 1
            })
            monthView.subviews.forEach({ (subview) in
                subview.isHidden = subview.tag == 2
            })
        }

        let layout = MSCollectionViewCalendarLayout()
        layout.sectionLayoutType = .horizontalTile
        let calendarVC = CalendarViewController(collectionViewLayout: layout)
        calendarVC.adminView = adminView
        calendarVC.startOfWeek = adminView ? self.selectedAdminDate : Date().beginningOfDay

        self.addChildViewController(calendarVC)
        calendarVC.view.frame = CGRect(
            x: 0,
            y: 0,
            width: scheduleView.frame.width,
            height: scheduleView.frame.height
        )

        scheduleView.addSubview(calendarVC.view)
        calendarVC.didMove(toParentViewController: self)
        self.calendarViewController = calendarVC
    }
    
    @IBAction func decreaseButtonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            selectedAdminDate = selectedAdminDate - 1.year
            break
        case 2:
            selectedAdminDate = selectedAdminDate - 1.month
            break
        case 3:
            selectedAdminDate = selectedAdminDate - 1.week
            break
        default:
            break
        }
        reloadData()
    }
    
    @IBAction func increaseButtonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            selectedAdminDate = selectedAdminDate + 1.year
            break
        case 2:
            selectedAdminDate = selectedAdminDate + 1.month
            break
        case 3:
            selectedAdminDate = selectedAdminDate + 1.week
            break
        default:
            break
        }
        reloadData()
    }

    fileprivate func reloadData() {
        trainingsService.trainingsForWeek(self.selectedAdminDate) {
            (response: [Training]?, error) in
            if let trainings: [Training] = response {

                self.calendarViewController?.startOfWeek = self.selectedAdminDate
                self.calendarViewController?.trainings = trainings
                self.calendarViewController?.reloadWeek()
                self.setupWeekLabelsForDate(self.selectedAdminDate)
            }
        }
    }

    @objc fileprivate func showParticipants() {
        weekSelectorView?.isHidden = true
    }

    @objc fileprivate func showWeekSelector() {
        weekSelectorView?.isHidden = !(weekSelectorView?.isHidden)!
    }

    func isDateAvailableToSelect(_ date: Date) -> Bool {
        return true
    }

    func calendarDidSelectDate(_ calendar: RYRCalendar, selectedDate: Date) {
        self.selectedAdminDate = selectedDate.beginningOfWeek.beginningOfDay+1.day
        self.weekSelectorView?.isHidden = true
        reloadData()
    }

    func calendarDidSelectMultipleDate(_ calendar: RYRCalendar,
                                       selectedStartDate startDate: Date, endDate: Date) {
    }

    func calendarDidScrollToMonth(_ calendar: RYRCalendar, monthDate: Date) {
    }

    @objc fileprivate func backToLogin() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func addTraining() {
        let addingVC = AddingTrainingViewController()
        addingVC.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(addingVC, animated: true)
    }

    //Workaround for bug in RYRcalendar
    internal func moveWeekSelectorToTrash() {
        if let weekSelectorView = weekSelectorView {
            let appDelegate: AppDelegate? = UIApplication.shared.delegate
                as? AppDelegate
            weekSelectorView.delegate = nil
            weekSelectorView.isHidden = true
            appDelegate?.window?.addSubview(weekSelectorView)
        }
    }
}
