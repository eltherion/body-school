//
//  TrainingListViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import QuartzCore
import Eureka
import Pantry

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l <= r
    default:
        return !(rhs < lhs)
    }
}

fileprivate func verticalFrame() -> CGRect { return CGRect(x: 0, y: 0, width: 82, height: 44)}
fileprivate func horizontalFrame() -> CGRect { return CGRect(x: 0, y: 0, width: 60, height: 32)}

class TrainingListViewController: UIViewController, LogoPresenter,
    UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    var logoView: UIImageView?
    let selectedAdminDate: Date = Date().beginningOfWeek.beginningOfDay
    let century = 36500

    fileprivate let trainingsService = TrainingsService()
    fileprivate let trainingsListTableView = UITableView(frame: .zero, style: .grouped)
    fileprivate var daysList: [String] = []
    fileprivate var trainingsDict: [String : [Training]] = [:]
    var adminView: Bool = false

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isTranslucent = false

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Menu", comment: ""),
            style: .plain,
            target: self,
            action: #selector(SSASideMenu.presentMenuViewController))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
        view.backgroundColor = UIColor.white
        
        if adminView {
            navigationItem.rightBarButtonItems = [
                UIBarButtonItem(customView: UIImageView(
                    image: UIImage(named: "logo_navigation_bar.png"))),
                UIBarButtonItem(
                    barButtonSystemItem: UIBarButtonSystemItem.add,
                    target: self,
                    action: #selector(addTraining))
            ]
            navigationItem.rightBarButtonItems?.forEach({ (barButtonItem) in
                barButtonItem.tintColor = UIColor.colorWithHexString("5BBC7A")
            })
            self.didRotate(from: UIApplication.shared.statusBarOrientation)
        }
        
        self.setupLayout()

        self.trainingsFromDate(self.selectedAdminDate, century)
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.trainingsFromDate(self.selectedAdminDate, century)
        super.viewDidAppear(animated)
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        let logoView = navigationItem.rightBarButtonItems?[0].customView
        if UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
            logoView?.frame = horizontalFrame()
        } else {
            logoView?.frame = verticalFrame()
        }
        self.trainingsListTableView.reloadData()
    }

    fileprivate func setupLayout() {
        self.view.addSubview(trainingsListTableView)
        trainingsListTableView.dataSource = self
        trainingsListTableView.delegate = self
        trainingsListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        if #available(iOS 9.0, *) {//
            trainingsListTableView.cellLayoutMarginsFollowReadableWidth = false//
        }
        
        let footerBackground = UIView()
        self.view.addSubview(footerBackground)
        footerBackground.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        
        let filterButton = UIButton()
        filterButton.setTitle(NSLocalizedString("Course filter", comment: ""), for: .normal)
        footerBackground.addSubview(filterButton)
        filterButton.tintColor = UIColor.white
        filterButton.addTarget(self, action: #selector(filterButtonTapped),
                               for: .touchUpInside)

        self.setupFooterConstraints(footerBackground)
        self.setupFilterButtonConstraints(filterButton, footerBackground: footerBackground)
        
        trainingsListTableView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.bottom.equalTo()(footerBackground.mas_top)
        }
    }

    fileprivate func setupFooterConstraints(_ footerBackground: UIView) {
        footerBackground.mas_makeConstraints { (make) in
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(44)
        }
    }

    fileprivate func setupFilterButtonConstraints(_ filterButton: UIButton, footerBackground: UIView) {
        filterButton.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(footerBackground.mas_top)
            let _ = make?.bottom.equalTo()(footerBackground.mas_bottom)
            let _ = make?.right.equalTo()(footerBackground.mas_right)
            let _ = make?.left.equalTo()(footerBackground.mas_left)
        }
    }

    fileprivate func trainingsFromDate(_ date: Date, _ days: Int) {
        trainingsService.trainingsFromDate(date, days: century) {
            (response: [Training]?, error) in
            if let fetchedTrainings: [Training] = response {
                let dateFormat = "yyyy-MM-dd"
                let startOfWeek = self.selectedAdminDate
                let endOfWeek = startOfWeek.endOfDay + 6.days
                let daysSet = NSMutableOrderedSet()
                var weekActivityCounter = 0
                
                self.daysList = []
                self.trainingsDict = [:]
                
                fetchedTrainings.forEach({ (t) in
                    if let dayString = t.startDate?.stringFromFormat(dateFormat) {
                        daysSet.add(dayString)
                        weekActivityCounter += startOfWeek < t.startDate && t.startDate < endOfWeek ? 1 : 0
                        if let arrayForDayCount = self.trainingsDict[dayString]?.count,
                            arrayForDayCount > 0 {
                            self.trainingsDict[dayString]?.append(t)
                        } else {
                            self.trainingsDict[dayString] = [t]
                        }
                    }
                })
                
                if let daysList = daysSet.array as? [String] {
                    self.daysList = daysList
                }
                
                self.trainingsListTableView.reloadData()
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.daysList.count
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        return self.daysList[section]
            .dateFromFormat("yyyy-MM-dd", locale: DateFormatter().locale)?
            .stringFromFormat("EEEE, dd MMMM YYYY")
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.text = self.daysList[section]
                .dateFromFormat("yyyy-MM-dd", locale: DateFormatter().locale)?
                .stringFromFormat("EEEE, dd MMMM YYYY")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let dayString = self.daysList[section]
        
        if let trainingsForDay = self.trainingsDict[dayString] {
            return trainingsForDay.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")

        let dayString = self.daysList[indexPath.section]
        
        if let trainingsForDay = self.trainingsDict[dayString],
            trainingsForDay.count > 0 {
            let training = trainingsForDay[indexPath.row]
            
            if let schemaCode = training.schematic?.schemaCode,
                let schemaName = training.schematic?.schemaName {
                let trainingNameLabel = UILabel()
                
                cell.textLabel?.text = " "
                cell.addSubview(trainingNameLabel)
                
                trainingNameLabel.text = schemaCode + " - " + schemaName
                trainingNameLabel.mas_makeConstraints { (make) in
                    let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
                    let _ = make?.left.equalTo()(cell.textLabel?.mas_left)
                    let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-118)
                    let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
                }
            }
            if let startDate = training.startDate {
                cell.detailTextLabel?.text = startDate.stringFromFormat("HH:mm")
            }
            cell.detailTextLabel?.textColor = UIColor.lightGray
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 18.0)
            
            if let isParticipant = training.isParticipant, isParticipant {
                cell.backgroundColor = UIColor.colorWithHexString("5BBC7A").withAlphaComponent(0.75)
            } else{
                cell.backgroundColor = UIColor.white
            }
            
            if let usersCount = training.usersCount,
                let maxUsersCount = training.schematic?.maxUsersCount {
                let currentString = "\(usersCount) / \(maxUsersCount)"
                let currentLabel = UILabel()
                
                cell.addSubview(currentLabel)
                
                currentLabel.text = currentString
                currentLabel.textAlignment = .center
                currentLabel.layer.masksToBounds = true
                currentLabel.layer.cornerRadius = 12
                currentLabel.mas_makeConstraints { (make) in
                    let _ = make?.top.equalTo()(cell.mas_top)?.offset()(10)
                    let _ = make?.left.equalTo()(cell.mas_right)?.offset()(-110)
                    let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-10)
                    let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-10)
                }
                
                if let isCanceled = training.isCanceled , isCanceled {
                    currentLabel.backgroundColor = UIColor.blue
                } else if training.usersCount < training.schematic?.minUsersCount {
                    currentLabel.backgroundColor = UIColor.yellow
                } else if training.schematic?.minUsersCount <= training.usersCount
                    && training.usersCount < training.schematic?.maxUsersCount {
                    currentLabel.backgroundColor = UIColor.green
                } else {
                    currentLabel.backgroundColor = UIColor.red
                }
            }
        }

        cell.selectionStyle = .none
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let dayString = self.daysList[indexPath.section]
        
        if let training = self.trainingsDict[dayString]?[indexPath.row],
            let startOfWeek = training.startDate?.beginningOfDay.beginningOfWeek {
            
            if adminView {
                let cancelingVC = CancelingViewController()
                cancelingVC.modalTransitionStyle = .crossDissolve
                cancelingVC.training = Training(training: training)
                self.navigationController?.pushViewController(cancelingVC, animated: true)
            } else {
                
                let endOfWeek = startOfWeek.endOfDay + 6.days
                let signedForCount = trainingsDict
                    .values
                    .joined()
                    .filter({$0.isParticipant == true
                        && startOfWeek <= $0.startDate
                        && $0.startDate <= endOfWeek})
                    .count
                
                let signingVC = SigningViewController()
                signingVC.modalTransitionStyle = .crossDissolve
                signingVC.training = Training(training: training)
                signingVC.signedTrainingsCount = signedForCount
                self.navigationController?.pushViewController(signingVC, animated: true)
            }
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let targetOffset = targetContentOffset.pointee
        if targetOffset.y == 0 && self.adminView {
            if let firstDate = self.daysList.first?.dateFromFormat("yyyy-MM-dd", locale: DateFormatter().locale) {
                self.trainingsFromDate(firstDate - 1.week, century + 7)
            } else {
                self.trainingsFromDate(self.selectedAdminDate, century)
            }
        }
    }
    
    @objc fileprivate func addTraining() {
        let addingVC = AddingTrainingViewController()
        addingVC.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(addingVC, animated: true)
    }

    @objc fileprivate func filterButtonTapped() {
        let schematicListViewController = SchematicListViewController()
        schematicListViewController.selectionMode = true
        self.navigationController?.pushViewController(schematicListViewController, animated: true)
    }
}
