//
//  MenuViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit

class MenuViewController: UIViewController, SSASideMenuDelegate {

    var adminView: Bool = false

    let adminTitles: [String] = [NSLocalizedString("Training list I", comment: ""),
                            NSLocalizedString("User list I", comment: ""),
                            NSLocalizedString("Resignation settings I", comment: ""),
                            NSLocalizedString("Holidays list I", comment: ""),
                            NSLocalizedString("Schematic list I", comment: ""),
                            NSLocalizedString("Training generation I", comment: ""),
                            NSLocalizedString("Change password I", comment: ""),
                            NSLocalizedString("Logout I", comment: "")]

    let adminSubtitles: [String] = [NSLocalizedString("Training list II", comment: ""),
                            NSLocalizedString("User list II", comment: ""),
                            NSLocalizedString("Resignation settings II", comment: ""),
                            NSLocalizedString("Holidays list II", comment: ""),
                            NSLocalizedString("Schematic list II", comment: ""),
                            NSLocalizedString("Training generation II", comment: ""),
                            NSLocalizedString("Change password II", comment: ""),
                            NSLocalizedString("Logout II", comment: "")]

    let adminImages: [String] = [NSLocalizedString("IconCalendar", comment: ""),
                            NSLocalizedString("IconProfile", comment: ""),
                            NSLocalizedString("IconSettings", comment: ""),
                            NSLocalizedString("IconSettings", comment: ""),
                            NSLocalizedString("IconSettings", comment: ""),
                            NSLocalizedString("IconSettings", comment: ""),
                            NSLocalizedString("IconSettings", comment: ""),
                            NSLocalizedString("IconHome", comment: "")]

    let userTitles: [String] = [NSLocalizedString("Training list I", comment: ""),
                                NSLocalizedString("Change password I", comment: ""),
                                NSLocalizedString("Logout I", comment: "")]

    let userSubtitles: [String] = [NSLocalizedString("Training list II", comment: ""),
                                   NSLocalizedString("Change password II", comment: ""),
                                   NSLocalizedString("Logout II", comment: "")]

    let userImages: [String] = [NSLocalizedString("IconCalendar", comment: ""),
                                 NSLocalizedString("IconCalendar", comment: ""),
                                 NSLocalizedString("IconSettings", comment: ""),
                                 NSLocalizedString("IconHome", comment: "")]

    var titles: [String]?
    var subtitles: [String]?
    var images: [String]?

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.frame = CGRect(x: 20,
                                 y: (self.view.frame.size.height - 54 * 5) / 2.0,
                                 width: self.view.frame.size.width,
                                 height: 54 * 5)
        tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isOpaque = false
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
        tableView.bounces = false
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.clear
        view.addSubview(tableView)
    }

    public func setViewType(_ isTrainer: Bool) {
        adminView =  isTrainer
        titles = adminView ? adminTitles : userTitles
        subtitles = adminView ? adminSubtitles : userSubtitles
        images = adminView ? adminImages : userImages
    }
}

// MARK : TableViewDataSource & Delegate Methods
extension MenuViewController: UITableViewDelegate, UITableViewDataSource {


    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return adminView ? 8 : 3
    }

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")

        cell.backgroundColor = UIColor.clear
        cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 17)
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text  = titles?[(indexPath as NSIndexPath).row]
        cell.detailTextLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
        cell.detailTextLabel?.textColor = UIColor.white
        cell.detailTextLabel?.text = subtitles?[(indexPath as NSIndexPath).row]
        if let imageName: String = images?[(indexPath as NSIndexPath).row] {
            cell.imageView?.image = UIImage(named: imageName)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.weekSelectorWorkaround()
        switch (indexPath as NSIndexPath).row {
        case 0:
            let trainingListViewController = TrainingListViewController()
            trainingListViewController.adminView = adminView
            let navViewController = UINavigationController(
                rootViewController: trainingListViewController)
            navViewController.modalTransitionStyle = .crossDissolve
            sideMenuViewController?.contentViewController =
            navViewController
            break
        case 1:
            if adminView {
                sideMenuViewController?.contentViewController =
                    UINavigationController(
                        rootViewController: UserListViewController()
                )
            } else {
                if let navigationVC = self.sideMenuViewController?
                    .contentViewController as? UINavigationController {
                    navigationVC
                        .pushViewController(ChangePasswordViewController(), animated: true)
                }
            }
            break
        case 2:
            if adminView {
                sideMenuViewController?.contentViewController =
                    UINavigationController(
                        rootViewController: SettingsViewController()
                )
                break
            } else {
                sideMenuViewController?.dismiss(animated: true, completion: nil)
            }
            break
        case 3:
                sideMenuViewController?.contentViewController =
                    UINavigationController(
                        rootViewController: EventListViewController()
                )
            break
        case 4:
            sideMenuViewController?.contentViewController =
                UINavigationController(
                    rootViewController: SchematicListViewController()
            )
            break
        case 5:
            sideMenuViewController?.contentViewController =
                UINavigationController(
                    rootViewController: GeneratingViewController()
            )
            break
        case 6:
            if let navigationVC = self.sideMenuViewController?
                .contentViewController as? UINavigationController {
                navigationVC
                    .pushViewController(ChangePasswordViewController(), animated: true)
            }
            break
        case 7:
            sideMenuViewController?.dismiss(animated: true, completion: nil)
            break
        default:
            break
        }
        sideMenuViewController?.hideMenuViewController()
    }

    //Workaround for bug in RYRcalendar
    fileprivate func weekSelectorWorkaround() {
        if let navVC: UINavigationController = sideMenuViewController?.contentViewController
            as? UINavigationController,
            let presentVC: WeekViewController = navVC.topViewController as? WeekViewController {
            presentVC.moveWeekSelectorToTrash()
        }
    }
}
