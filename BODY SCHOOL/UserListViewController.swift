//
//  UserListViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import Pantry

class UserListViewController: UIViewController, LogoPresenter,
    UITableViewDataSource, UITableViewDelegate {

    var logoView: UIImageView?
    fileprivate let loginService = LoginService()
    fileprivate let userListTableView = UITableView(frame: .zero, style: .plain)
    fileprivate var userList: [User] = []
    var selectionMode: Bool = false
    var selectedUsersList: [User] = []
    var userSelectionDelegate: UserSelectionDelegate? = nil

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isTranslucent = false

        if selectionMode {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: NSLocalizedString("Done", comment: ""),
                style: .plain,
                target: self,
                action: #selector(usersSelectedTapped))
            navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
            navigationController?.navigationBar.tintColor = UIColor.colorWithHexString("5BBC7A")
        } else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: NSLocalizedString("Menu", comment: ""),
                style: .plain,
                target: self,
                action: #selector(SSASideMenu.presentMenuViewController))
            navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
        }
        self.setLogoFrame(navigationItem)
        view.backgroundColor = UIColor.white

        self.setupLayout()

        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.fetchUserList()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    fileprivate func setupLayout() {
        let headerBackground = UIView()
        self.view.addSubview(headerBackground)
        headerBackground.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        self.setupHeaderConstraints(headerBackground)

        let addButton = UIButton(type: .contactAdd)
        if !selectionMode {
            headerBackground.addSubview(addButton)
            addButton.tintColor = UIColor.white
            addButton.addTarget(self, action: #selector(addNewUserTapped),
                                for: .touchUpInside)
            self.setupAddButtonConstraints(addButton, headerBackground: headerBackground)
        }

        let nameLabel = UILabel()
        headerBackground.addSubview(nameLabel)
        nameLabel.text = NSLocalizedString("Name & login", comment: "")
        nameLabel.textColor = UIColor.white
        nameLabel.textAlignment = .left
        self.setupNameLabelConstraints(nameLabel, headerBackground: headerBackground)

        let emailLabel = UILabel()
        headerBackground.addSubview(emailLabel)
        emailLabel.text = NSLocalizedString("Email", comment: "")
        emailLabel.textColor = UIColor.white
        emailLabel.textAlignment = .left
        emailLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(188)
            let _ = make?.right.equalTo()(self.selectionMode
                ? self.view.mas_right
                : addButton.mas_left)?
                .offset()(-8)
        }

        self.view.addSubview(userListTableView)
        userListTableView.dataSource = self
        userListTableView.delegate = self
        userListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        if #available(iOS 9.0, *) {//
            userListTableView.cellLayoutMarginsFollowReadableWidth = false//
        }
        userListTableView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)
        }
        
        if selectionMode {
            userListTableView.tintColor = UIColor.colorWithHexString("5BBC7A")
        }
    }

    fileprivate func setupHeaderConstraints(_ headerBackground: UIView) {
        headerBackground.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(44)
        }
    }

    fileprivate func setupAddButtonConstraints(_ addButton: UIButton, headerBackground: UIView) {
        addButton.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?
                .offset()(-70)
        }
    }

    fileprivate func setupNameLabelConstraints(_ nameLabel: UILabel, headerBackground: UIView) {
        nameLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(16)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(180)
        }
    }

    fileprivate func fetchUserList() {
        if let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            loginService.fetchUserList(token, completion: { (usersArray, error) in
                if let users: [User] = usersArray {
                    self.userList = users
                    self.userListTableView.reloadData()
                }
            })
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = userList[(indexPath as NSIndexPath).row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")

        cell.detailTextLabel?.text  = user.username
        cell.detailTextLabel?.textColor = UIColor.colorWithHexString("5BBC7A")
        if let firstName = user.firstName, let lastName = user.lastName {
            cell.textLabel?.text  = firstName + " " + lastName
        }
        cell.selectionStyle = .none
        
        let emailLabel = UILabel()
        cell.addSubview(emailLabel)
        emailLabel.text = user.email
        emailLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
            let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
            let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-56)
            let _ = make?.left.equalTo()(cell.mas_left)?.offset()(188)
        }

        if !selectionMode {

            let editUserButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            editUserButton.setImage(UIImage(named: "edit.png"), for: UIControlState())
            editUserButton.tag = (indexPath as NSIndexPath).row
            editUserButton.addTarget(self, action: #selector(editUserTapped),
                                      for: .touchUpInside)

            let removeUserButton = UIButton(frame: CGRect(x: 48, y: 0, width: 40, height: 40))
            removeUserButton.setImage(UIImage(named: "remove_user.png"), for: UIControlState())
            removeUserButton.tag = (indexPath as NSIndexPath).row
            removeUserButton.addTarget(self, action: #selector(removeUserTapped),
                                       for: .touchUpInside)

            let view = UIView(frame: CGRect(x: 0, y: 0, width: 88, height: 40))
            view.addSubview(editUserButton)
            view.addSubview(removeUserButton)
            
            cell.accessoryView = view
        } else {
            if selectedUsersList.contains(where: {$0.identifier == user.identifier}) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = userList[indexPath.row]
        if let index = selectedUsersList.index(where: {$0.identifier == user.identifier}) {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
            selectedUsersList.remove(at: index)
        } else {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            selectedUsersList.append(user)
        }
    }

    @objc fileprivate func editUserTapped(_ sender: UIButton) {
        let addingUserViewController = AddingUserViewController()
        addingUserViewController.user = userList[sender.tag]
        self.navigationController?.pushViewController(addingUserViewController, animated: true)
    }

    @objc fileprivate func removeUserTapped(_ sender: UIButton) {
        let user = userList[sender.tag]
        let userFullName: String
        if let firstName = user.firstName, let lastName = user.lastName {
            userFullName = firstName + " " + lastName + "?"
        } else {
            userFullName = "?"
        }
        let questionString: String = NSLocalizedString("Do you want to delete user ", comment: "")
        let messageString: String = questionString + userFullName

        let alertVC = UIAlertController(title: NSLocalizedString("Confirmation", comment: ""),
                                        message: messageString,
                                        preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""),
                                         style: .cancel) { (action) in
            // ...
        }
        alertVC.addAction(cancelAction)

        let OKAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""),
                                     style: .default) { (action) in
            if let identifier = user.identifier, let user: User = Pantry.unpack("lastUser"),
                                        let token = user.token {
                self.loginService.deleteUserWithIdentifier(identifier,
                                                      token: token,
                                                      completion: { (response, error) in
                    self.fetchUserList()
                })
            }
        }
        alertVC.addAction(OKAction)

        self.present(alertVC, animated: true, completion: nil)
    }

    @objc fileprivate func addNewUserTapped() {
        let addingUserViewController = AddingUserViewController()
        self.navigationController?.pushViewController(addingUserViewController, animated: true)
    }
    
    @objc fileprivate func usersSelectedTapped() {
        self.userSelectionDelegate?.didSelectUsers(users: selectedUsersList)
        let _ = self.navigationController?.popViewController(animated: true)
    }

}
