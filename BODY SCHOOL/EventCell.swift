//
//  EventCell.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit
import Timepiece

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

class EventCell: UICollectionViewCell {

    var training: Training?
    var borderView: UIView
    var titleLabel: UILabel
    var identifierLabel: UILabel
    var startTimeLabel: UILabel
    var minLabel: UILabel
    var maxLabel: UILabel
    var currentLabel: UILabel
    var intSelected: Bool = false

    // MARK: - UIView
    override init(frame: CGRect) {

        self.borderView = UIView()
        self.titleLabel = UILabel()
        self.identifierLabel = UILabel()
        self.startTimeLabel = UILabel()
        self.minLabel = UILabel()
        self.maxLabel = UILabel()
        self.currentLabel = UILabel()

        super.init(frame: frame)
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.shouldRasterize = true
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowRadius = 5.0
        self.layer.shadowOpacity = 0.0

        self.contentView.addSubview(self.borderView)

        self.titleLabel.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.titleLabel)

        self.identifierLabel.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.identifierLabel)

        self.startTimeLabel.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.startTimeLabel)

        self.minLabel.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.minLabel)

        
        self.maxLabel.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.maxLabel)

        self.currentLabel.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.currentLabel)

        self.updateColors()
        self.setupConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupConstraints() {
        let contentMargin: CGFloat = 2.0
        let contentPadding: UIEdgeInsets = UIEdgeInsets(
            top: 2.0,
            left: 5.0,
            bottom: 2.0,
            right: 5.0
        )
        self.borderView.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.height.equalTo()(self.mas_height)
            let _ = make?.width.equalTo()(self.mas_width)
            let _ = make?.left.equalTo()(self.mas_left)
            let _ = make?.top.equalTo()(self.mas_top)
        }
        self.titleLabel.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.top.equalTo()(self.mas_top)?.offset()(contentPadding.top)
            let _ = make?.left.equalTo()(self.mas_left)?.offset()(contentPadding.left)
            let _ = make?.right.equalTo()(self.mas_right)?.offset()(-contentPadding.right)
        }
        self.identifierLabel.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.top.equalTo()(self.titleLabel.mas_bottom)?.offset()(contentMargin)
            let _ = make?.left.equalTo()(self.mas_left)?.offset()(contentPadding.left)
            let _ = make?.right.equalTo()(self.mas_centerX)?.offset()(-contentPadding.right)
        }
        self.startTimeLabel.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.top.equalTo()(self.titleLabel.mas_bottom)?.offset()(contentMargin)
            let _ = make?.left.equalTo()(self.mas_centerX)?.offset()(contentPadding.left)
            let _ = make?.right.equalTo()(self.mas_right)?.offset()(-contentPadding.right)
        }
        self.minLabel.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.top.equalTo()(self.identifierLabel.mas_bottom)?.offset()(contentMargin)
            let _ = make?.left.equalTo()(self.mas_left)?.offset()(contentPadding.left)
            let _ = make?.right.equalTo()(self.mas_centerX)?.offset()(-contentPadding.right-40)
            let _ = make?.bottom.lessThanOrEqualTo()(self.mas_bottom)?.offset()(-contentPadding.bottom)
        }
        self.maxLabel.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.top.equalTo()(self.identifierLabel.mas_bottom)?.offset()(contentMargin)
            let _ = make?.left.equalTo()(self.mas_centerX)?.offset()(contentPadding.right-45)
            let _ = make?.right.equalTo()(self.mas_centerX)?.offset()(-contentPadding.left+45)
            let _ = make?.bottom.lessThanOrEqualTo()(self.mas_bottom)?.offset()(-contentPadding.bottom)
        }
        self.currentLabel.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.top.equalTo()(self.identifierLabel.mas_bottom)?.offset()(contentMargin)
            let _ = make?.left.equalTo()(self.mas_centerX)?.offset()(contentPadding.left+40)
            let _ = make?.right.equalTo()(self.mas_right)?.offset()(-contentPadding.right)
            let _ = make?.bottom.lessThanOrEqualTo()(self.mas_bottom)?.offset()(-contentPadding.bottom)
        }
    }

    // MARK: - UICollectionViewCell
    override internal var isSelected: Bool {
        get {
        return intSelected
        }
        set {
            intSelected = newValue
            super.isSelected = newValue
            // Must be here for animation to fire
            self.updateColors()
        }
    }

    // MARK: - EventCell
    func training(_ training: Training) {
        self.training = training
        if let isParticipant = training.isParticipant {
            self.isSelected = isParticipant
        }
        let boldAttributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]

        //title label
        if let schemaCode = training.schematic?.schemaCode,
            let schemaName = training.schematic?.schemaName {
            self.titleLabel.attributedText = NSAttributedString(
                string: "\(schemaCode) \(schemaName)",
                attributes: self.titleAttributesHighlighted(self.isSelected)
            )
        }

        //identifier label
//        if let identifier = training.identifier {
//            let identifierString = "Id: \(identifier)" as NSString
//            let identifierRange = identifierString.range(of: "\(identifier)")
//            let identifierLabelString = NSMutableAttributedString(
//                string: identifierString as String,
//                attributes: self.subtitleAttributesHighlighted(self.isSelected, alignment: .left)
//            )
//            identifierLabelString.addAttributes(boldAttributes, range: identifierRange)
//            self.identifierLabel.attributedText = identifierLabelString
//        }
        self.identifierLabel.text = " "

        //start time label
        if let startDate = training.startDate {
            let startTimeLabelString = NSMutableAttributedString(
                string: startDate.stringFromFormat("HH:mm ")
                    + NSLocalizedString("hour", comment: ""),
                attributes: self.subtitleAttributesHighlighted(self.isSelected, alignment: .right)
            )
            let startTimeRange = NSRange(location: 0, length: startTimeLabelString.length)
            startTimeLabelString.addAttributes(boldAttributes, range: startTimeRange)
            self.startTimeLabel.attributedText = startTimeLabelString
        }

        setupCounterLabels(training, boldAttributes: boldAttributes)
    }

    func setupCounterLabels(_ training: Training, boldAttributes: [String: AnyObject]) {

        //min label
        if let minUsersCount = training.schematic?.minUsersCount {
            let minString = "\(NSLocalizedString("min: ", comment:""))\(minUsersCount)" as NSString
            let minRange = minString.range(of: "\(minUsersCount)")
            let minLabelString = NSMutableAttributedString(
                string: minString as String,
                attributes: self.subtitleAttributesHighlighted(self.isSelected, alignment: .left)
            )
            minLabelString.addAttributes(boldAttributes, range: minRange)
            self.minLabel.attributedText = minLabelString
        }

        //max label
        if let maxUsersCount = training.schematic?.maxUsersCount {
            let maxString = "\(NSLocalizedString("max: ", comment:""))\(maxUsersCount)" as NSString
            let maxRange = maxString.range(of: "\(maxUsersCount)")
            let maxLabelString = NSMutableAttributedString(
                string: maxString as String,
                attributes: self.subtitleAttributesHighlighted(self.isSelected, alignment: .center)
            )
            maxLabelString.addAttributes(boldAttributes, range: maxRange)
            self.maxLabel.attributedText = maxLabelString
        }

        //current label
        if let usersCount = training.usersCount {
            let currentString =
                "\(NSLocalizedString("cur.:  ", comment:""))\(usersCount)" as NSString
            let currentRange = currentString.range(of: "  \(usersCount)")
            let currentLabelString = NSMutableAttributedString(
                string: currentString as String,
                attributes: self.subtitleAttributesHighlighted(self.isSelected, alignment: .right)
            )
            currentLabelString.addAttributes(boldAttributes, range: currentRange)
            if let isCanceled = training.isCanceled , isCanceled {
                currentLabelString.addAttribute(NSBackgroundColorAttributeName,
                                                value: UIColor.blue,
                                                range: currentRange)
            } else if training.usersCount < training.schematic?.minUsersCount {
                currentLabelString.addAttribute(NSBackgroundColorAttributeName,
                                                value: UIColor.yellow,
                                                range: currentRange)
            } else if training.schematic?.minUsersCount <= training.usersCount
                && training.usersCount < training.schematic?.maxUsersCount {
                currentLabelString.addAttribute(NSBackgroundColorAttributeName,
                                                value: UIColor.green,
                                                range: currentRange)
            } else {
                currentLabelString.addAttribute(NSBackgroundColorAttributeName,
                                                value: UIColor.red,
                                                range: currentRange)
            }
            self.currentLabel.attributedText = currentLabelString
        }
    }

    func updateColors() {
        self.contentView.layer.borderColor = UIColor.darkGray.cgColor
        self.contentView.layer.borderWidth = CGFloat(1.0)
        self.borderView.backgroundColor = self.backgroundColorHighlighted(self.isSelected)
        self.titleLabel.textColor = UIColor.black
        self.identifierLabel.textColor = UIColor.black
        self.startTimeLabel.textColor = UIColor.black
        self.minLabel.textColor = UIColor.black
        self.maxLabel.textColor = UIColor.black
        self.currentLabel.textColor = UIColor.black

    }

    func titleAttributesHighlighted(_ highlighted: Bool) -> [String : AnyObject] {
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.hyphenationFactor = 1.0
        paragraphStyle.lineBreakMode = .byTruncatingTail
        return [NSFontAttributeName:UIFont.boldSystemFont(ofSize: 14.0),
                NSForegroundColorAttributeName:UIColor.black,
                NSParagraphStyleAttributeName:paragraphStyle]
    }

    func subtitleAttributesHighlighted(_ highlighted: Bool,
                                       alignment: NSTextAlignment) -> [String : AnyObject] {
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        paragraphStyle.hyphenationFactor = 1.0
        paragraphStyle.lineBreakMode = .byTruncatingTail
        return [NSFontAttributeName:UIFont.systemFont(ofSize: 14.0),
                NSForegroundColorAttributeName:UIColor.black,
                NSParagraphStyleAttributeName:paragraphStyle]
    }

    func backgroundColorHighlighted(_ selected: Bool) -> UIColor {
        return selected
            ? UIColor.colorWithHexString("5BBC7A").withAlphaComponent(0.75)
            : UIColor.lightGray.withAlphaComponent(0.2)
    }
}
