//
//  ChangePasswordCredentials.swift
//  BODY SCHOOL
//
//  Created by eltherion on 8/2/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation

class ChangePasswordCredentials: JSONAble {
    
    var password: String
    var passwordRepeat: String
    
    init(_ pasword: String, _ passwordRepeat: String) {
        self.password = pasword
        self.passwordRepeat = passwordRepeat
    }
}
