//
//  AddingTrainingViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import Timepiece
import Pantry

class AddingTrainingViewController: UIViewController, LogoPresenter,
    UITableViewDataSource, UITableViewDelegate, UITextViewDelegate,
    SMDatePickerDelegate {

    var logoView: UIImageView?
    fileprivate let schematicService = SchematicService()
    fileprivate let trainingsService = TrainingsService()
    fileprivate let dateFormatter = DateFormatter()
    fileprivate let timeLabel = UILabel()
    fileprivate let picker = SMDatePicker()
    fileprivate let changeDateButton = UIButton(type: .custom)
    fileprivate let schematicListTableView = UITableView(frame: .zero, style: .plain)
    fileprivate let newTraining = Training()
    var schematicList: [Schematic] = []

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isTranslucent = false

        self.setLogoFrame(navigationItem)
        self.view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Cancel", comment: ""),
            style: .plain,
            target: self,
            action: #selector(cancelButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")

        self.setupLayout()

        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.fetchSchematicList()
        picker.showPickerInView(view, animated: true)
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
        if !picker.isHidden {
            picker.showPickerInView(view, animated: true)
        }
    }

    fileprivate func setupLayout() {
        
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        
        picker.pickerDate = picker.pickerDate.beginningOfHour + 1.hour
        picker.toolbarBackgroundColor = UIColor.colorWithHexString("5BBC7A")
        picker.pickerBackgroundColor = UIColor.white
        picker.leftButtons = []
        picker.rightButtons.forEach { (item) in
            item.tintColor = UIColor.white
        }
        picker.delegate = self
        self.newTraining.startDate = self.picker.pickerDate
        
        self.view.addSubview(timeLabel)
        timeLabel.text = dateFormatter.string(from: picker.pickerDate as Date)
        self.setupTimeLabelConstraints(timeLabel)
        
        self.view.addSubview(changeDateButton)
        changeDateButton.isHidden = true
        changeDateButton.setTitle(NSLocalizedString("Change date", comment: ""),
                                  for: UIControlState())
        changeDateButton.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        changeDateButton.setTitleColor(UIColor.white, for: UIControlState())
        changeDateButton.addTarget(self, action: #selector(changeDateButtonTapped),
                                   for: .touchUpInside)
        self.setupChangeDateButtonConstraints(changeDateButton)
        
        let descriptionLabel = UILabel()
        self.view.addSubview(descriptionLabel)
        descriptionLabel.text = NSLocalizedString("Description", comment: "")
        self.setupDescriptionLabelConstraints(descriptionLabel)

        let descriptionTextView = UITextView()
        self.view.addSubview(descriptionTextView)
        descriptionTextView.tag = 1
        descriptionTextView.isEditable = false
        descriptionTextView.isScrollEnabled = true
        descriptionTextView.isSelectable = true
        descriptionTextView.delegate = self
        descriptionTextView.text = NSLocalizedString("Tap item to display its description.",
                                                     comment: "")
        descriptionTextView.font = UIFont.systemFont(ofSize: 17)
        descriptionTextView.textColor = UIColor.lightGray
        self.setupDescriptionTextViewConstraints(descriptionTextView,
                                                 descriptionLabel: descriptionLabel)

        let headerBackground = UIView()
        self.view.addSubview(headerBackground)
        headerBackground.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        self.setupHeaderConstraints(headerBackground, descriptionTextView: descriptionTextView)

        let codeLabel = UILabel()
        headerBackground.addSubview(codeLabel)
        codeLabel.text = NSLocalizedString("Code", comment: "")
        codeLabel.textColor = UIColor.white
        codeLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_left)?.offset()(76)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(16)
        }

        let minLabel = UILabel()
        headerBackground.addSubview(minLabel)
        minLabel.text = NSLocalizedString("Min", comment: "")
        minLabel.textColor = UIColor.white
        minLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)?.offset()(-56)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?.offset()(-86)
        }

        let maxLabel = UILabel()
        headerBackground.addSubview(maxLabel)
        maxLabel.text = NSLocalizedString("Max", comment: "")
        maxLabel.textColor = UIColor.white
        maxLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)?.offset()(-8)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?.offset()(-48)
        }
        
        let nameLabel = UILabel()
        headerBackground.addSubview(nameLabel)
        nameLabel.text = NSLocalizedString("Name", comment: "")
        nameLabel.textColor = UIColor.white
        self.setupNameLabelConstraints(nameLabel, headerBackground: headerBackground,
                                       minLabel: minLabel)

        self.view.addSubview(schematicListTableView)
        schematicListTableView.dataSource = self
        schematicListTableView.delegate = self
        schematicListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        schematicListTableView.layer.borderColor = UIColor.colorWithHexString("5BBC7A").cgColor
        schematicListTableView.layer.borderWidth = 0.5
        if #available(iOS 9.0, *) {//
            schematicListTableView.cellLayoutMarginsFollowReadableWidth = false//
        }
        schematicListTableView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)?.offset()(-60)
        }
        
        let saveButton = UIButton()
        view.addSubview(saveButton)
        saveButton.backgroundColor = UIColor.green.withAlphaComponent(0.8)
        saveButton.setTitleColor(UIColor.black, for: UIControlState())
        saveButton.setTitle(NSLocalizedString("Save changes", comment: ""),
                            for: UIControlState())
        saveButton.addTarget(self,
                             action: #selector(saveChanges),
                             for: .touchUpInside)
        saveButton.mas_makeConstraints { (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.centerX.equalTo()(self.view.mas_centerX)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)?.offset()(-8)
        }
    }

    fileprivate func setupTimeLabelConstraints(_ timeLabel: UILabel) {
        timeLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)?.offset()(8)
            let _ = make?.left.equalTo()(self.view.mas_left)?.offset()(8)
            let _ = make?.right.equalTo()(self.view.mas_right)?.offset()(-8)
            let _ = make?.height.equalTo()(32)
        }
    }
    
    fileprivate func setupChangeDateButtonConstraints(_ changeDateButton: UIButton) {
        changeDateButton.mas_makeConstraints { (make) in
            let _ = make?.width.equalTo()(150)
            let _ = make?.height.equalTo()(32)
            let _ = make?.right.equalTo()(self.view.mas_right)?.offset()(-8)
            let _ = make?.top.equalTo()(self.view.mas_top)?.offset()(8)
        }
    }

    fileprivate func setupDescriptionLabelConstraints(_ descriptionLabel: UILabel) {
        descriptionLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)?.offset()(48)
            let _ = make?.left.equalTo()(self.view.mas_left)?.offset()(8)
            let _ = make?.right.equalTo()(self.view.mas_right)?.offset()(-8)
            let _ = make?.height.equalTo()(32)
        }
    }

    fileprivate func setupDescriptionTextViewConstraints(_ descriptionTextView: UITextView,
                                                     descriptionLabel: UILabel) {
        descriptionTextView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(descriptionLabel.mas_bottom)?.offset()(8)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(62)
        }
    }

    fileprivate func setupHeaderConstraints(_ headerBackground: UIView, descriptionTextView: UITextView) {
        headerBackground.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(descriptionTextView.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(44)
        }
    }

    fileprivate func setupNameLabelConstraints(_ nameLabel: UILabel, headerBackground: UIView,
                                           minLabel: UILabel) {
        nameLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(minLabel.mas_left)?.offset()(-8)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(84)
        }
    }

    fileprivate func fetchSchematicList() {
        if let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            schematicService.fetchSchematicListFiltered(true, token: token,
                                                        completion: { (schematicsArray, error) in
                if let schematics: [Schematic] = schematicsArray {
                    self.schematicList = schematics
                    self.schematicListTableView.reloadData()
                }
            })
        }
    }
    
    func datePicker(_ picker: SMDatePicker, didPickDate date: Date) {
        timeLabel.text = dateFormatter.string(from: date)
        newTraining.startDate = date
    }
    
    func datePickerDidDisappear(_ picker: SMDatePicker) {
        changeDateButton.isHidden = !changeDateButton.isHidden
        picker.isHidden = !picker.isHidden    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schematicList.count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schematic = schematicList[(indexPath as NSIndexPath).row]
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")

        cell.textLabel?.text = schematic.schemaCode

        let nameLabel = UILabel()
        cell.addSubview(nameLabel)
        nameLabel.text = schematic.schemaName
        nameLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
            let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
            let _ = make?.left.equalTo()(cell.mas_left)?.offset()(82)
            let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-94)
        }

        if let min = schematic.minUsersCount {
            let minLabel = UILabel()
            cell.addSubview(minLabel)
            minLabel.text = String(min)
            minLabel.mas_makeConstraints { (make) in
                let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
                let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
                let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-56)
                let _ = make?.left.equalTo()(cell.mas_right)?.offset()(-86)
            }
        }

        if let max = schematic.maxUsersCount {
            let maxLabel = UILabel()
            cell.addSubview(maxLabel)
            maxLabel.text = String(max)
            maxLabel.mas_makeConstraints { (make) in
                let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
                let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
                let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-8)
                let _ = make?.left.equalTo()(cell.mas_right)?.offset()(-48)
            }
        }

        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.colorWithHexString("5BBC7A")
                                             .withAlphaComponent(0.5)
        cell.selectedBackgroundView = bgColorView

        return cell
    }
    
    @objc fileprivate func changeDateButtonTapped(_ changeDateButton: UIButton) {
        changeDateButton.isHidden = !changeDateButton.isHidden
        picker.isHidden = !picker.isHidden
        picker.showPickerInView(view, animated: true)
    }
    
    @objc fileprivate func saveChanges() {
        if let startDate = newTraining.startDate,
            let schematicIdentifier = newTraining.schematic?.identifier,
            let user: User = Pantry.unpack("lastUser") {
            if Date() < startDate {
                let training = Training()
                training.startDate = startDate
                training.schematic = Schematic()
                training.schematic?.identifier = schematicIdentifier
                trainingsService.saveTraining(user, training: training,
                    completion: { (addedTraining, error, responseData) -> Void in
                        if let _ = addedTraining {
                            let _ = self.navigationController?.popViewController(animated: true)
                        } else if let data = responseData,
                            let errorString = String(data:data, encoding: String.Encoding.utf8) {
                            self.showError(errorString)
                        }
                })
            } else {
                showError(NSLocalizedString("Selected training date lies in the past.",
                                            comment: ""))
            }
        } else {
            showError(NSLocalizedString("Select training type.",
                                        comment: ""))
        }
    }
    
    fileprivate func showError(_ errorString: String) {
        UIAlertView(
            title: NSLocalizedString("Error", comment: ""),
            message: errorString,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schematic = schematicList[(indexPath as NSIndexPath).row]
        self.newTraining.schematic = schematic
        if let descriptionTextView: UITextView = self.view.viewWithTag(1) as? UITextView {
            descriptionTextView.text = schematic.descr
            descriptionTextView.textColor = UIColor.black
        }
    }
    
    @objc fileprivate func cancelButtonTapped() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
