//
//  EventListViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import Timepiece
import Pantry
import AKPickerView

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

class EventListViewController: UIViewController, LogoPresenter,
    AKPickerViewDataSource, AKPickerViewDelegate,
    UITableViewDataSource, UITableViewDelegate {

    var logoView: UIImageView?
    fileprivate let eventsService = EventsService()
    fileprivate let pickerView: AKPickerView = AKPickerView(frame: .zero)
    fileprivate let eventListTableView = UITableView(frame: .zero, style: .plain)
    fileprivate let dateFormatter = DateFormatter()
    var eventList: [Event] = []

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isTranslucent = false

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Menu", comment: ""),
            style: .plain,
            target: self,
            action: #selector(SSASideMenu.presentMenuViewController))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
        self.setLogoFrame(navigationItem)
        view.backgroundColor = UIColor.white

        dateFormatter.dateFormat = "dd-MM-YYYY"

        self.setupLayout()

        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.fetchEventList()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    fileprivate func setupLayout() {

        let changeYearButton = UIButton(type: .custom)
        self.view.addSubview(changeYearButton)
        changeYearButton.setTitle(NSLocalizedString("Change year", comment: ""),
                                  for: UIControlState())
        changeYearButton.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        changeYearButton.setTitleColor(UIColor.white, for: UIControlState())
        changeYearButton.addTarget(self, action: #selector(changeYearButtonTapped),
                       for: .touchUpInside)
        changeYearButton.mas_makeConstraints { (make) in
            let _ = make?.width.equalTo()(250)
            let _ = make?.height.equalTo()(30)
            let _ = make?.centerX.equalTo()(self.view.mas_centerX)
            let _ = make?.top.equalTo()(20)
        }

        let headerBackground = UIView()
        self.view.addSubview(headerBackground)
        headerBackground.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        self.setupHeaderConstraints(headerBackground)

        self.pickerView.isHidden = true
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.view.addSubview(self.pickerView)
        self.pickerView.highlightedTextColor = UIColor.colorWithHexString("5BBC7A")
        self.pickerView.highlightedFont = UIFont.boldSystemFont(ofSize: 25)
        self.pickerView.interitemSpacing = 10
        self.pickerView.pickerViewStyle = .wheel
        self.pickerView.selectItem(Date().year-2000)
        pickerView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(changeYearButton.mas_bottom)?.offset()(8)
            let _ = make?.bottom.equalTo()(headerBackground.mas_top)?.offset()(-8)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
        }

        self.pickerView.reloadData()

        let addButton = UIButton(type: .contactAdd)
        headerBackground.addSubview(addButton)
        addButton.tintColor = UIColor.white
        addButton.addTarget(self, action: #selector(addNewEventTapped),
                            for: .touchUpInside)
        self.setupAddButtonConstraints(addButton, headerBackground: headerBackground)

        let nameLabel = UILabel()
        headerBackground.addSubview(nameLabel)
        nameLabel.text = NSLocalizedString("Event name", comment: "")
        nameLabel.textColor = UIColor.white
        nameLabel.textAlignment = .left
        self.setupNameLabelConstraints(nameLabel, headerBackground: headerBackground,
                                       addButton: addButton)

        let loginabel = UILabel()
        headerBackground.addSubview(loginabel)
        loginabel.text = NSLocalizedString("Date", comment: "")
        loginabel.textColor = UIColor.white
        loginabel.textAlignment = .center
        loginabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_left)?.offset()(105)
            let _ = make?.left.equalTo()(headerBackground.mas_left)
        }

        self.view.addSubview(eventListTableView)
        eventListTableView.dataSource = self
        eventListTableView.delegate = self
        eventListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        if #available(iOS 9.0, *) {//
            eventListTableView.cellLayoutMarginsFollowReadableWidth = false//
        }
        eventListTableView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)
        }
    }

    fileprivate func setupHeaderConstraints(_ headerBackground: UIView) {
        headerBackground.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)?.offset()(110)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(44)
        }
    }

    fileprivate func setupAddButtonConstraints(_ addButton: UIButton, headerBackground: UIView) {
        addButton.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?
                .offset()(-70)
        }
    }

    fileprivate func setupNameLabelConstraints(_ nameLabel: UILabel, headerBackground: UIView,
                                           addButton: UIButton) {
        nameLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(addButton.mas_left)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(113)
        }
    }

    fileprivate func fetchEventList() {
        if let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            eventsService.fetchEventsList(token, completion: { (eventsArray, error) in
                if let events: [Event] = eventsArray {
                    self.eventList = events.sorted(by: { (e1, e2) -> Bool in
                        e1.dateTime < e2.dateTime
                    })
                    self.eventListTableView.reloadData()
                }
            })
        }
    }

    func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
        return 101
    }

    func pickerView(_ pickerView: AKPickerView, titleForItem item: Int) -> String {
        return "\(2000+item)"
    }

    func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
        if let firstEventYear = self.eventList.first?.dateTime?.year {
            let selectedYear = 2000+item
            if selectedYear < firstEventYear {
                self.eventListTableView.scrollToRow(
                    at: IndexPath(item: 0, section: 0),
                    at: .top, animated: true)
            } else if let index = self.eventList.index(where: {$0.dateTime?.year == (2000+item)}) {
                self.eventListTableView.scrollToRow(
                    at: IndexPath(item: index, section: 0),
                    at: .top, animated: true)
            } else {
                self.eventListTableView.scrollToRow(
                    at: IndexPath(item: self.eventList.count - 1, section: 0),
                    at: .bottom, animated: true)
            }
        }
        self.pickerView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventList.count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = eventList[(indexPath as NSIndexPath).row]
        let cell = UITableViewCell(style: .value2, reuseIdentifier: "cell")

        if let date = event.dateTime {
            cell.textLabel?.text = dateFormatter.string(from: date as Date)
        }
        cell.textLabel?.textColor = UIColor.colorWithHexString("5BBC7A")
        cell.detailTextLabel?.text = event.eventName
        cell.selectionStyle = .none

        let editEventButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        editEventButton.setImage(UIImage(named: "edit.png"), for: UIControlState())
        editEventButton.tag = (indexPath as NSIndexPath).row
        editEventButton.addTarget(self, action: #selector(editEventTapped),
                       for: .touchUpInside)

        let removeEventButton = UIButton(frame: CGRect(x: 48, y: 0, width: 40, height: 40))
        removeEventButton.setImage(UIImage(named: "remove.png"), for: UIControlState())
        removeEventButton.tag = (indexPath as NSIndexPath).row
        removeEventButton.addTarget(self, action: #selector(removeEventTapped),
                                   for: .touchUpInside)

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 88, height: 40))
        view.addSubview(editEventButton)
        view.addSubview(removeEventButton)

        cell.accessoryView = view

        return cell
    }

    @objc fileprivate func removeEventTapped(_ sender: UIButton) {
        let event = eventList[sender.tag]
        let eventFullName: String
        if let eventName = event.eventName {
            eventFullName = eventName + "?"
        } else {
            eventFullName = "?"
        }
        let questionString: String = NSLocalizedString("Do you want to delete event ", comment: "")
        let messageString: String = questionString + eventFullName

        let alertVC = UIAlertController(title: NSLocalizedString("Confirmation", comment: ""),
                                        message: messageString,
                                        preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""),
                                         style: .cancel) { (action) in
            // ...
        }
        alertVC.addAction(cancelAction)

        let OKAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""),
                                     style: .default) { (action) in
            if let identifier = event.identifier, let user: User = Pantry.unpack("lastUser"),
                                        let token = user.token {
                self.eventsService.deleteEventWithIdentifier(identifier,
                                                      token: token,
                                                      completion: { (response, error) in
                    self.fetchEventList()
                })
            }
        }
        alertVC.addAction(OKAction)

        self.present(alertVC, animated: true, completion: nil)
    }

    @objc fileprivate func changeYearButtonTapped(_ sender: UIButton) {
        self.pickerView.isHidden = !self.pickerView.isHidden
        sender.setTitle(
            self.pickerView.isHidden ?
                NSLocalizedString("Change year", comment: "")
                : NSLocalizedString("OK", comment: ""),
            for: UIControlState())
    }

    @objc fileprivate func addNewEventTapped() {
        let addingEventViewController = AddingEventViewController()
        self.navigationController?.pushViewController(addingEventViewController, animated: true)
    }

    @objc fileprivate func editEventTapped(_ sender: UIButton) {
        let addingEventViewController = AddingEventViewController()
        addingEventViewController.event = eventList[sender.tag]
        self.navigationController?.pushViewController(addingEventViewController, animated: true)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
