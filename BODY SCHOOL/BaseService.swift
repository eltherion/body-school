//
//  BaseService.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation

protocol ServiceProtocol {
    static func endpoint() -> String
}

class BaseService {
    static func serverURL() -> String { return "http://46.248.164.29" }
}
