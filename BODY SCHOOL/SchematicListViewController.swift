//
//  SchematicListViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import Timepiece
import Pantry

class SchematicListViewController: UIViewController, LogoPresenter,
    UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {

    var logoView: UIImageView?
    fileprivate let schematicService = SchematicService()
    fileprivate let schematicListTableView = UITableView(frame: .zero, style: .plain)
    var schematicList: [Schematic] = []
    var selectionMode: Bool = false
    var selectedCoursesList: [Schematic] = []

    override func viewDidLoad() {
        self.navigationController?.navigationBar.isTranslucent = false

        if selectionMode {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: NSLocalizedString("Done", comment: ""),
                style: .plain,
                target: self,
                action: #selector(coursesSelectedTapped))
            navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
            navigationController?.navigationBar.tintColor = UIColor.colorWithHexString("5BBC7A")
        } else {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                title: NSLocalizedString("Menu", comment: ""),
                style: .plain,
                target: self,
                action: #selector(SSASideMenu.presentMenuViewController))
            navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")
            self.setLogoFrame(navigationItem)
        }
        view.backgroundColor = UIColor.white
        
        self.setupLayout()

        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.fetchSchematicList()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    fileprivate func setupLayout() {
        let descriptionTextView = UITextView()
        self.view.addSubview(descriptionTextView)

        if !selectionMode {
            let descriptionLabel = UILabel()
            self.view.addSubview(descriptionLabel)
            descriptionLabel.text = NSLocalizedString("Description", comment: "")
            self.setupDescriptionLabelConstraints(descriptionLabel)

            descriptionTextView.tag = 1
            descriptionTextView.isEditable = false
            descriptionTextView.isScrollEnabled = true
            descriptionTextView.isSelectable = true
            descriptionTextView.delegate = self
            descriptionTextView.text = NSLocalizedString("Tap item to display its description.",
                                                         comment: "")
            descriptionTextView.font = UIFont.systemFont(ofSize: 17)
            descriptionTextView.textColor = UIColor.lightGray
            self.setupDescriptionTextViewConstraints(descriptionTextView,
                                                     descriptionLabel: descriptionLabel)
        }

        let headerBackground = UIView()
        self.view.addSubview(headerBackground)
        headerBackground.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        self.setupHeaderConstraints(headerBackground, descriptionTextView: descriptionTextView)

        let addButton = UIButton(type: .contactAdd)
        headerBackground.addSubview(addButton)
        addButton.tintColor = UIColor.white
        addButton.addTarget(self, action: #selector(addNewSchematicTapped),
                            for: .touchUpInside)
        self.setupAddButtonConstraints(addButton, headerBackground: headerBackground)

        let codeLabel = UILabel()
        headerBackground.addSubview(codeLabel)
        codeLabel.text = NSLocalizedString("Code", comment: "")
        codeLabel.textColor = UIColor.white
        codeLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_left)?.offset()(76)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(16)
        }

        let nameLabel = UILabel()
        headerBackground.addSubview(nameLabel)
        nameLabel.text = NSLocalizedString("Name", comment: "")
        nameLabel.textColor = UIColor.white
        self.setupNameLabelConstraints(nameLabel, headerBackground: headerBackground,
                                       addButton: addButton)

        let minLabel = UILabel()
        headerBackground.addSubview(minLabel)
        minLabel.text = NSLocalizedString("Min", comment: "")
        minLabel.textColor = UIColor.white
        minLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)?.offset()(-144)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?.offset()(-174)
        }

        let maxLabel = UILabel()
        headerBackground.addSubview(maxLabel)
        maxLabel.text = NSLocalizedString("Max", comment: "")
        maxLabel.textColor = UIColor.white
        maxLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)?.offset()(-96)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?.offset()(-136)
        }

        self.view.addSubview(schematicListTableView)
        schematicListTableView.dataSource = self
        schematicListTableView.delegate = self
        schematicListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        if #available(iOS 9.0, *) {//
            schematicListTableView.cellLayoutMarginsFollowReadableWidth = false//
        }
        schematicListTableView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.bottom.equalTo()(self.view.mas_bottom)
        }
        
        if selectionMode {
            schematicListTableView.tintColor = UIColor.colorWithHexString("5BBC7A")
        }
        
        minLabel.isHidden = selectionMode
        maxLabel.isHidden = selectionMode
        addButton.isHidden = selectionMode
    }

    fileprivate func setupDescriptionLabelConstraints(_ descriptionLabel: UILabel) {
        descriptionLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(self.view.mas_top)?.offset()(8)
            let _ = make?.left.equalTo()(self.view.mas_left)?.offset()(8)
            let _ = make?.right.equalTo()(self.view.mas_right)?.offset()(-8)
            let _ = make?.height.equalTo()(32)
        }
    }

    fileprivate func setupDescriptionTextViewConstraints(_ descriptionTextView: UITextView,
                                                     descriptionLabel: UILabel) {
        descriptionTextView.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(descriptionLabel.mas_bottom)?.offset()(8)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(62)
        }
    }

    fileprivate func setupHeaderConstraints(_ headerBackground: UIView, descriptionTextView: UITextView) {
        headerBackground.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(descriptionTextView.mas_bottom)
            let _ = make?.left.equalTo()(self.view.mas_left)
            let _ = make?.right.equalTo()(self.view.mas_right)
            let _ = make?.height.equalTo()(44)
        }
    }

    fileprivate func setupAddButtonConstraints(_ addButton: UIButton, headerBackground: UIView) {
        addButton.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(headerBackground.mas_right)
            let _ = make?.left.equalTo()(headerBackground.mas_right)?
                .offset()(-70)
        }
    }

    fileprivate func setupNameLabelConstraints(_ nameLabel: UILabel, headerBackground: UIView,
                                           addButton: UIButton) {
        nameLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(headerBackground.mas_top)
            let _ = make?.bottom.equalTo()(headerBackground.mas_bottom)
            let _ = make?.right.equalTo()(addButton.mas_left)?.offset()(-104)
            let _ = make?.left.equalTo()(headerBackground.mas_left)?.offset()(84)
        }
    }

    fileprivate func fetchSchematicList() {
        if let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            schematicService.fetchSchematicListFiltered(selectionMode, token: token,
                                                completion: { (schematicsArray, error) in
                if let schematics: [Schematic] = schematicsArray {
                    self.schematicList = schematics
                    self.selectedCoursesList = schematics.filter({ (s) -> Bool in
                        if let inFilters = s.inFilters {
                            return inFilters
                        } else {
                            return false
                        }
                    })
                    self.schematicListTableView.reloadData()
                }
            })
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schematicList.count
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schematic = schematicList[(indexPath as NSIndexPath).row]
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")

        cell.textLabel?.text = schematic.schemaCode

        let nameLabel = UILabel()
        cell.addSubview(nameLabel)
        nameLabel.text = schematic.schemaName
        nameLabel.mas_makeConstraints { (make) in
            let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
            let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
            let _ = make?.left.equalTo()(cell.mas_left)?.offset()(82)
            let _ = make?.right.equalTo()(cell.mas_right)?.offset()(self.selectionMode ? -8 : -182)
        }

        if !selectionMode {
            if let min = schematic.minUsersCount {
                let minLabel = UILabel()
                cell.addSubview(minLabel)
                minLabel.text = String(min)
                minLabel.mas_makeConstraints { (make) in
                    let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
                    let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
                    let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-144)
                    let _ = make?.left.equalTo()(cell.mas_right)?.offset()(-174)
                }
            }

            if let max = schematic.maxUsersCount {
                let maxLabel = UILabel()
                cell.addSubview(maxLabel)
                maxLabel.text = String(max)
                maxLabel.mas_makeConstraints { (make) in
                    let _ = make?.top.equalTo()(cell.textLabel?.mas_top)
                    let _ = make?.bottom.equalTo()(cell.textLabel?.mas_bottom)
                    let _ = make?.right.equalTo()(cell.mas_right)?.offset()(-96)
                    let _ = make?.left.equalTo()(cell.mas_right)?.offset()(-136)
                }
            }

            let bgColorView = UIView()
            bgColorView.backgroundColor = UIColor.colorWithHexString("5BBC7A")
                                                .withAlphaComponent(0.5)
            cell.selectedBackgroundView = bgColorView

            let editSchematicButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            editSchematicButton.setImage(UIImage(named: "edit.png"), for: UIControlState())
            editSchematicButton.tag = (indexPath as NSIndexPath).row
            editSchematicButton.addTarget(self, action: #selector(editSchematicTapped),
                           for: .touchUpInside)

            let removeSchematicButton = UIButton(frame: CGRect(x: 48, y: 0, width: 40, height: 40))
            removeSchematicButton.setImage(UIImage(named: "remove.png"), for: UIControlState())
            removeSchematicButton.tag = (indexPath as NSIndexPath).row
            removeSchematicButton.addTarget(self, action: #selector(removeSchematicTapped),
                                       for: .touchUpInside)

            let view = UIView(frame: CGRect(x: 0, y: 0, width: 88, height: 40))
            view.addSubview(editSchematicButton)
            view.addSubview(removeSchematicButton)

            cell.accessoryView = view
        } else {
            cell.selectionStyle = .none
            
            if selectedCoursesList.contains(where: {$0.identifier == schematic.identifier}) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schematic = schematicList[(indexPath as NSIndexPath).row]
        if !selectionMode, let descriptionTextView: UITextView = self.view.viewWithTag(1) as? UITextView {
            descriptionTextView.text = schematic.descr
            descriptionTextView.textColor = UIColor.black
        }
        if let index = selectedCoursesList.index(where: {$0.identifier == schematic.identifier}) {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
            selectedCoursesList.remove(at: index)
        } else {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            selectedCoursesList.append(schematic)
        }
    }

    @objc fileprivate func removeSchematicTapped(_ sender: UIButton) {
        let schematic = schematicList[sender.tag]
        let schematicFullName: String
        if let schematicName = schematic.schemaName {
            schematicFullName = schematicName
        } else {
            schematicFullName = ""
        }
        let questionString: String = NSLocalizedString("Do you want to delete \\(schematicFullName) schematic?",
                                                       comment: "")
        let messageString: String = questionString.replacingOccurrences(of: "\\(schematicFullName)",
                                                                        with: schematicFullName
        )

        let alertVC = UIAlertController(title: NSLocalizedString("Confirmation", comment: ""),
                                        message: messageString,
                                        preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""),
                                         style: .cancel) { (action) in
            // ...
        }
        alertVC.addAction(cancelAction)

        let OKAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""),
                                     style: .default) { (action) in
            if let identifier = schematic.identifier, let user: User = Pantry.unpack("lastUser"),
                                        let token = user.token {
                self.schematicService.deleteSchematicWithIdentifier(identifier,
                                                      token: token,
                                                      completion: { (response, error) in
                    if let responseString = response,
                        responseString == "Schema nicht löschbar, da es bereits bei Kursen verwendet wird" {
                        UIAlertView(title: "Error",
                                    message: "Item has linked trainings.",
                                    delegate: nil,
                                    cancelButtonTitle: "OK")
                        .show()
                    }
                    self.fetchSchematicList()
                })
            }
        }
        alertVC.addAction(OKAction)

        self.present(alertVC, animated: true, completion: nil)
    }

    @objc fileprivate func addNewSchematicTapped() {
        let addingSchematicViewController = AddingSchematicViewController()
        self.navigationController?.pushViewController(addingSchematicViewController, animated: true)
    }

    @objc fileprivate func editSchematicTapped(_ sender: UIButton) {
        let addingSchematicViewController = AddingSchematicViewController()
        addingSchematicViewController.schematic = schematicList[sender.tag]
        self.navigationController?.pushViewController(addingSchematicViewController, animated: true)
    }
    
    @objc fileprivate func coursesSelectedTapped() {
        if let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            
            let identifiers = (selectedCoursesList.count == 0 ? [] : selectedCoursesList)
                .map { (s) -> [String: AnyObject] in
                if let identifier = s.identifier as AnyObject? {
                    return ["id":  identifier]
                } else {
                    return [:]
                }
            }
                
            schematicService.saveSchematicFilters(identifiers, token: token, completion: { (error) in
                let _ = self.navigationController?.popViewController(animated: true)
            })
        }
    }
}
