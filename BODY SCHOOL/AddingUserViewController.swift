//
//  AddingUserViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import SwiftValidate
import Pantry
import ARSLineProgress

class AddingUserViewController: FormViewController, LogoPresenter, UITextViewDelegate {

    fileprivate let loginService = LoginService()
    fileprivate var validation: ValidatorChain?

    var logoView: UIImageView?
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setLogoFrame(navigationItem)

        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Cancel", comment: ""),
            style: .plain,
            target: self,
            action: #selector(cancelChanges))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")

        validation = setupValidation()
        setupView(validation!)
    }

    override func viewWillAppear(_ animated: Bool) {

        if let firstName: String = self.user?.firstName,
            let lastName: String = self.user?.lastName,
            let login: String = self.user?.username,
            let email: String = self.user?.email,
            let firstNameRow: AccountFloatLabelRow = self.form.rowBy(tag: "First Name Field"),
            let lastNameRow: AccountFloatLabelRow = self.form.rowBy(tag: "Last Name Field"),
            let loginRow: AccountFloatLabelRow = self.form.rowBy(tag: "Login Field"),
            let emailRow: AccountFloatLabelRow = self.form.rowBy(tag: "Email Field") {
            firstNameRow.value = firstName
            firstNameRow.baseCell.update()
            lastNameRow.value = lastName
            lastNameRow.baseCell.update()
            loginRow.value = login
            loginRow.baseCell.update()
            emailRow.value = email
            emailRow.baseCell.update()
        }

        super.viewWillAppear(animated)
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.validation = nil
        super.viewWillDisappear(animated)
    }

    fileprivate func setupValidation() -> ValidatorChain {
        return ValidatorChain() {
            $0.stopOnFirstError = true
            $0.stopOnException = true
            } <~~ ValidatorRequired() {
                $0.errorMessage = NSLocalizedString("Mandatory field", comment: "")
        }
    }

    func setupView(_ validation: ValidatorChain) {
        form +++ Section()

            <<< self.addAccountFloatLabelRow("First Name Field",
                    title: NSLocalizedString("First name", comment: ""),
                    firstResponder: true)

            <<< self.addAccountFloatLabelRow("Last Name Field",
                     title: NSLocalizedString("Last name", comment: ""),
                     firstResponder: false)

            <<< self.addAccountFloatLabelRow("Login Field",
                     title: NSLocalizedString("Login", comment: ""),
                     firstResponder: false)
            
            <<< self.addAccountFloatLabelRow("Email Field",
                     title: NSLocalizedString("Email", comment: ""),
                     firstResponder: false)

            +++ Section()

            <<< SchemeRow().cellSetup({ (cell, row) in
                self.setupSchemeRow(cell)
            })
    }

    fileprivate func addAccountFloatLabelRow(_ tag: String, title: String, firstResponder: Bool) ->
        AccountFloatLabelRow {
        return AccountFloatLabelRow(tag) {
            $0.title = title
            $0.cell.textField.returnKeyType = UIReturnKeyType.done
            $0.cell.textField.enablesReturnKeyAutomatically = true
            if firstResponder {$0.cell.textField.becomeFirstResponder()}
            }.onCellHighlightChanged({ (cell, row) in
                if !cell.textField.isFirstResponder {
                    if let validation = self.validation {
                        let validationResults = self.isValidRow(validation,
                            floatRow: row)
                        if !validationResults.valid {
                            self.showError(validationResults.titleString,
                                errorString: validationResults.errorString)
                        }
                    }
                }
            }).cellUpdate({ cell, row in
                cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                if (self.user != nil && row.tag == "Login Field") {
                    cell.textField.isEnabled = false
                }
            })
    }

    fileprivate func showError(_ rowName: String?, errorString: String) {
        UIAlertView(
            title: rowName != nil ? NSLocalizedString(rowName!, comment: "") : "",
            message: errorString,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    fileprivate func isValidRow(_ validation: ValidatorChain,
                            floatRow: AccountFloatLabelRow?) ->
        (valid: Bool, titleString: String, errorString: String) {

            if let row = floatRow {

                let _ = validation.validate(row.value, context: nil)

                if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                    let titleString = row.title,
                    let errorString = validator?.errors.first {
                    return (false, titleString, errorString)
                }
            }
        return(true, "", "")
    }

    fileprivate func lockUI(_ lock: Bool) {

        form.allRows.forEach { (b) in
            b.disabled = Condition(booleanLiteral: lock)
            b.hidden = Condition(booleanLiteral: lock)
            b.evaluateDisabled()
            b.evaluateHidden()
        }
    }

    // MARK: - text view delegate methods
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }

    // MARK: - table/form view delegate methods
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 {
            return 54
        } else {
            return 60
        }
    }

    func tableView(_ tableView: UITableView,
                   shouldHighlightRowAtIndexPath indexPath: IndexPath) -> Bool {
        return false
    }

    // MARK: - custom scheme row initialization
    fileprivate func setupSchemeRow(_ cell: UIView) {
        setupRowButtons(cell)
    }

    fileprivate func setupRowButtons(_ cell: UIView) {
        let saveButton = UIButton()
        saveButton.backgroundColor = UIColor.green.withAlphaComponent(0.8)
        saveButton.setTitleColor(UIColor.black, for: UIControlState())
        saveButton.setTitle(NSLocalizedString("Save changes", comment: ""),
                            for: UIControlState())
        saveButton.addTarget(self,
                             action: #selector(saveChanges),
                             for: .touchUpInside)

        let cancelButton = UIButton()
        cancelButton.backgroundColor = UIColor.orange
        cancelButton.setTitleColor(UIColor.black, for: UIControlState())
        cancelButton.titleLabel?.numberOfLines = 0
        cancelButton.titleLabel?.lineBreakMode = .byWordWrapping
        cancelButton.titleLabel?.textAlignment = .center
        cancelButton.setTitle(NSLocalizedString("Cancel without changes", comment: ""),
                              for: UIControlState())
        cancelButton.addTarget(self,
                               action: #selector(cancelChanges),
                               for: .touchUpInside)

        cell.addSubview(saveButton)
        cell.addSubview(cancelButton)

        saveButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.right.equalTo()(cell.mas_centerX)?.offset()(-4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })

        cancelButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.left.equalTo()(cell.mas_centerX)?.offset()(4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })
    }

    @objc fileprivate func saveChanges() {
        self.trySaveUser(form.values(), showErrors: true)
    }

    fileprivate func trySaveUser(_ values: [String: Any?], showErrors: Bool) {
        if let firstNameString = values["First Name Field"] as? String,
            let lastNameString = values["Last Name Field"] as? String,
            let loginString = values["Login Field"] as? String,
            let emailString = values["Email Field"] as? String,
            let user: User = Pantry.unpack("lastUser"),
            let token = user.token {

            ARSLineProgress.show()
            lockUI(true)

            let userToSave = User()
            userToSave.identifier = self.user?.identifier
            userToSave.username = loginString
            userToSave.firstName = firstNameString
            userToSave.lastName = lastNameString
            userToSave.email = emailString

            loginService
                .saveUser(userToSave, edit: self.user != nil, token: token, completion: { (user, error) in
                    if user != nil {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showSuccess()
                                self.lockUI(false)
                                let _ = self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showFail()
                                self.lockUI(false)
                                self.showError("", errorString: (error?.localizedDescription)!)
                        })
                    }
                })
        } else {
            if showErrors {
                showError("", errorString: NSLocalizedString("Fill all fields.", comment: ""))
            }
        }
    }

    @objc fileprivate func cancelChanges() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
