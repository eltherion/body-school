//
//  TimeRowHeaderBackground.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit

class TimeRowHeaderBackground: UICollectionReusableView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
