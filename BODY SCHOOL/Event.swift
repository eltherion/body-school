//
//  Event.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/8/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: Mappable {

    var identifier: Int?
    var eventName: String?
    var dateTime: Date?

    init() {

    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        identifier <- map["id"]
        eventName <- map["eventName"]

        let stringOptional = String(describing: map.JSON["date"])
        let dateString = stringOptional
            .replacingOccurrences(of: "Optional(", with: "")
            .replacingOccurrences(of: "000)", with: "")
        if let dateInt = UInt64(dateString) {
            let dateTime = Date(timeIntervalSince1970: TimeInterval(dateInt))
            self.dateTime = dateTime

        }
    }

    func toDictionary() -> [String : AnyObject] {
        var mutableDict: [String: AnyObject] = [:]
        if let identifier = self.identifier { mutableDict["id"] = identifier as AnyObject? }
        if let eventName = self.eventName { mutableDict["eventName"] = eventName as AnyObject? }
        if let dateTime = self.dateTime {
            let date = Double(UInt64(dateTime.timeIntervalSince1970) * UInt64(1000))
            mutableDict["date"] = date as AnyObject?
        }
        return mutableDict
    }
}
