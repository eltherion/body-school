//
//  SettingsViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import SwiftValidate
import Pantry
import ARSLineProgress

let TERM: String = "TERM_OF_APPEAL_PARTICIPATE"
let START: String = "BEGINNING_OF_THE_PERIOD_OF_MONITORING_ATTENDANCE_TRAINING"
let END: String = "THE_END_OF_THE_PERIOD_OF_MONITORING_ATTENDANCE_TRAINING"
let FREQUENCY: String = "THE_FREQUENCY_OF_MONITORING_ATTENDANCE"

class SettingsViewController: FormViewController, LogoPresenter {

    fileprivate let settingsService = SettingsService()
    fileprivate var validation: ValidatorChain?

    var logoView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isTranslucent = false

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Menu", comment: ""),
            style: .plain,
            target: self,
            action: #selector(SSASideMenu.presentMenuViewController))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")

        self.setLogoFrame(navigationItem)

        view.backgroundColor = UIColor.white

        validation = setupValidation()
        setupView(validation!)
    }

    override func viewDidAppear(_ animated: Bool) {
        self.fetchSettings()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.validation = nil
        super.viewWillDisappear(animated)
    }

    fileprivate func setupValidation() -> ValidatorChain {
        return ValidatorChain() {
            $0.stopOnFirstError = true
            $0.stopOnException = true
            } <~~ ValidatorRequired() {
                $0.errorMessage = NSLocalizedString("Mandatory field", comment: "")
        }
    }

    func setupView(_ validation: ValidatorChain) {
        form +++ Section()

            <<< self.addAccountFloatLabelRow(TERM,
                    title: NSLocalizedString("Term of class attendance resignation", comment: ""),
                    firstResponder: false)

            <<< self.addAccountFloatLabelRow(START,
                    title: NSLocalizedString("Start of the training attendance monitoring period",
                        comment: ""),
                    firstResponder: false)

            <<< self.addAccountFloatLabelRow(END,
                    title: NSLocalizedString("End of the training attendance monitoring period",
                        comment: ""),
                    firstResponder: false)

            <<< self.addAccountFloatLabelRow(FREQUENCY,
                    title: NSLocalizedString("Attendance monitoring frequency", comment: ""),
                    firstResponder: false)

            +++ Section()

            <<< SchemeRow().cellSetup({ (cell, row) in
                self.setupSchemeRow(cell)
            })
    }

    fileprivate func addAccountFloatLabelRow(_ tag: String, title: String, firstResponder: Bool) ->
        AccountFloatLabelRow {
        return AccountFloatLabelRow(tag) {
            $0.title = title
            $0.cell.textField.returnKeyType = UIReturnKeyType.done
            $0.cell.textField.enablesReturnKeyAutomatically = true
            if firstResponder {$0.cell.textField.becomeFirstResponder()}
            }.onCellHighlightChanged({ (cell, row) in
                if let validation = self.validation {
                    let validationResults = self.isValidRow(validation,
                        floatRow: row)
                    if !validationResults.valid {
                        self.showError(validationResults.titleString,
                            errorString: validationResults.errorString)
                    }
                }
            }).cellUpdate({ cell, row in
                cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
            })
    }

    fileprivate func fetchSettings() {
        if let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            settingsService.fetchSettings(token, completion: { (settingsArray, error) in
                if let settings: [Setting] = settingsArray {
                    settings.forEach {
                        if let name: String = $0.name,
                            let floatRow: AccountFloatLabelRow = self.form.rowBy(tag: name)
                                as? AccountFloatLabelRow,
                            let floatCell = floatRow.baseCell as? AccountFloatLabelCell {
                            floatRow.value = $0.value
                            floatCell.update()
                        }
                    }
                }
            })
        }
    }

    fileprivate func showError(_ rowName: String?, errorString: String) {
        UIAlertView(
            title: rowName != nil ? NSLocalizedString(rowName!, comment: "") : "",
            message: errorString,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    fileprivate func isValidRow(_ validation: ValidatorChain,
                            floatRow: AccountFloatLabelRow?) ->
        (valid: Bool, titleString: String, errorString: String) {

            if let row = floatRow {

                let _ = validation.validate(row.value, context: nil)

                if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                    let titleString = row.title,
                    let errorString = validator?.errors.first {
                    return (false, titleString, errorString)
                }
            }
            return (true, "", "")
    }

    fileprivate func lockUI(_ lock: Bool) {

        form.allRows.forEach { (b) in
            b.disabled = Condition(booleanLiteral: lock)
            b.hidden = Condition(booleanLiteral: lock)
            b.evaluateDisabled()
            b.evaluateHidden()
        }
    }

    // MARK: - text view delegate methods
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }

    // MARK: - table/form view delegate methods
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 {
            return 54
        } else {
            return 60
        }
    }

    func tableView(_ tableView: UITableView,
                   shouldHighlightRowAtIndexPath indexPath: IndexPath) -> Bool {
        return false
    }

    // MARK: - custom scheme row initialization
    fileprivate func setupSchemeRow(_ cell: UIView) {
        setupRowButtons(cell)
    }

    fileprivate func setupRowButtons(_ cell: UIView) {
        let saveButton = UIButton()
        saveButton.backgroundColor = UIColor.green.withAlphaComponent(0.8)
        saveButton.setTitleColor(UIColor.black, for: UIControlState())
        saveButton.setTitle(NSLocalizedString("Save changes", comment: ""),
                            for: UIControlState())
        saveButton.addTarget(self,
                             action: #selector(saveChanges),
                             for: .touchUpInside)

        let cancelButton = UIButton()
        cancelButton.backgroundColor = UIColor.orange
        cancelButton.setTitleColor(UIColor.black, for: UIControlState())
        cancelButton.titleLabel?.numberOfLines = 0
        cancelButton.titleLabel?.lineBreakMode = .byWordWrapping
        cancelButton.titleLabel?.textAlignment = .center
        cancelButton.setTitle(NSLocalizedString("Cancel without changes", comment: ""),
                              for: UIControlState())
        cancelButton.addTarget(self,
                               action: #selector(cancelChanges),
                               for: .touchUpInside)

        cell.addSubview(saveButton)
        cell.addSubview(cancelButton)

        saveButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.right.equalTo()(cell.mas_centerX)?.offset()(-4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })

        cancelButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.left.equalTo()(cell.mas_centerX)?.offset()(4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })
    }

    @objc fileprivate func saveChanges() {
        self.trySaveSettings(form.values(), showErrors: true)
    }

    fileprivate func trySaveSettings(_ values: [String: Any?], showErrors: Bool) {
        if let term = values[TERM] as? String,
            let start = values[START] as? String,
            let end = values[END] as? String,
            let frequency = values[FREQUENCY] as? String,
            let user: User = Pantry.unpack("lastUser"),
            let token = user.token {
            let settings = [
                Setting(name: TERM, value: term).toDictionary(),
                Setting(name: START, value: start).toDictionary(),
                Setting(name: END, value: end).toDictionary(),
                Setting(name: FREQUENCY, value: frequency).toDictionary()
            ]

            ARSLineProgress.show()
            lockUI(true)

            settingsService
                .saveSettings(settings, token: token, completion: { (error) in
                    if error == nil {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showSuccess()
                                self.lockUI(false)
                                self.fetchSettings()
                        })
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showFail()
                                self.lockUI(false)
                                self.showError("", errorString: (error?.localizedDescription)!)
                        })
                    }
                })
        } else {
            if showErrors {
                showError("", errorString: NSLocalizedString("Fill all fields.", comment: ""))
            }
        }
    }

    @objc fileprivate func cancelChanges() {
//        self.navigationController?.popViewControllerAnimated(true)
        self.perform(#selector(SSASideMenu.presentMenuViewController))
    }
}
