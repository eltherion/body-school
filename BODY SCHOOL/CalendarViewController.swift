//
//  CalendarViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit
import Timepiece
import Pantry

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

let eventCellReuseIdentifier: String = "EventCellReuseIdentifvar"
let holidayCellReuseIdentifier: String = "HolidayCellReuseIdentifvar"
let dayColumnHeaderReuseIdentifier: String = "DayColumnHeaderReuseIdentifier"
let timeRowHeaderReuseIdentifier: String = "TimeRowHeaderReuseIdentifier"

class CalendarViewController: UICollectionViewController, MSCollectionViewDelegateCalendarLayout {

    let collectionViewCalendarLayout: MSCollectionViewCalendarLayout
    let eventsService = EventsService()
    var startOfWeek: Date
    var trainings: [Training] = []
    var eventsDict: [String: Event] = [:]
    var adminView: Bool = false

    override init(collectionViewLayout layout: UICollectionViewLayout) {
        if let collectionLayout: MSCollectionViewCalendarLayout =
            layout as? MSCollectionViewCalendarLayout {
            self.collectionViewCalendarLayout = collectionLayout
        } else {
            self.collectionViewCalendarLayout = MSCollectionViewCalendarLayout()
            self.collectionViewCalendarLayout.sectionLayoutType = .horizontalTile
        }
            self.startOfWeek = Date().beginningOfDay
        super.init(collectionViewLayout: layout)
        self.collectionViewCalendarLayout.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.collectionView?.backgroundColor = UIColor.white
        self.collectionView?.bounces = false
        self.collectionView?.register(
            EventCell.self,
            forCellWithReuseIdentifier:eventCellReuseIdentifier
        )
        self.collectionView?.register(
            UICollectionViewCell.self,
            forCellWithReuseIdentifier:holidayCellReuseIdentifier
        )
        self.collectionView?.register(
            DayColumnHeader.self,
            forSupplementaryViewOfKind: MSCollectionElementKindDayColumnHeader,
            withReuseIdentifier: dayColumnHeaderReuseIdentifier
        )
        self.collectionView?.register(
            TimeRowHeader.self,
            forSupplementaryViewOfKind: MSCollectionElementKindTimeRowHeader,
            withReuseIdentifier: timeRowHeaderReuseIdentifier
        )
        self.collectionViewCalendarLayout.sectionWidth = self.layoutSectionWidth()
        // These are optional. If you don't want any of the decoration views,
        // just don't register a class for them.
         self.collectionViewCalendarLayout.register(
            CurrentTimeIndicator.self,
            forDecorationViewOfKind:MSCollectionElementKindCurrentTimeIndicator
        )
         self.collectionViewCalendarLayout.register(
            CurrentTimeGridline.self,
            forDecorationViewOfKind:MSCollectionElementKindCurrentTimeHorizontalGridline
        )
         self.collectionViewCalendarLayout.register(
            Gridline.self, forDecorationViewOfKind:MSCollectionElementKindVerticalGridline
        )
         self.collectionViewCalendarLayout.register(
            Gridline.self, forDecorationViewOfKind:MSCollectionElementKindHorizontalGridline
        )
         self.collectionViewCalendarLayout.register(
            TimeRowHeaderBackground.self,
            forDecorationViewOfKind:MSCollectionElementKindTimeRowHeaderBackground
        )
         self.collectionViewCalendarLayout.register(
            DayColumnHeaderBackground.self,
            forDecorationViewOfKind:MSCollectionElementKindDayColumnHeaderBackground
        )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.reloadWeek()
//        self.collectionViewCalendarLayout.invalidateLayoutCache()
//        self.collectionView?.reloadData()
//        self.collectionViewCalendarLayout
//            .scrollCollectionViewToClosetSectionToCurrentTimeAnimated(animated)
    }

    override func willTransition(
        to newCollection: UITraitCollection,
        with coordinator: UIViewControllerTransitionCoordinator) {
        // Ensure that collection view properly rotates between layouts
        self.collectionView?.collectionViewLayout.invalidateLayout()
        self.collectionViewCalendarLayout.invalidateLayoutCache()
        coordinator.animate(alongsideTransition: {
            (context: UIViewControllerTransitionCoordinatorContext) in
            self.collectionViewCalendarLayout.sectionWidth = self.layoutSectionWidth()

            }, completion: { (context: UIViewControllerTransitionCoordinatorContext) in
                self.collectionView?.reloadData()

        })
    }
    
    func reloadWeek() {
        if let user: User = Pantry.unpack("lastUser"),  
            let token = user.token {
            eventsService.fetchEventsForWeek(self.startOfWeek.stringFromFormat("yyyy-MM-dd"), token,
                                                 completion: { (eventsArray, error) in
                if let events: [Event] = eventsArray {
                    events.forEach({ (e) in
                        self.eventsDict[(e.dateTime?.stringFromFormat("yyyy-MM-dd"))!] = e
                    })
                    self.collectionViewCalendarLayout.invalidateLayoutCache()
                    self.collectionView?.reloadData()
                    self.collectionViewCalendarLayout
                        .collectionView?.setContentOffset(CGPoint.zero, animated: true)
                }
            })
        }
    }

    // MARK: - MSCalendarViewController
    func layoutSectionWidth() -> CGFloat {

        return CGFloat(254.0)
    }

    // MARK: - UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 7
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        let isHoliday = self.eventsDict[(startOfWeek + section.days).stringFromFormat("yyyy-MM-dd")] != nil
        
        return isHoliday ? 1 : trainings.filter({ (t) -> Bool in
            startOfWeek + section.days <= t.startDate
                && t.startDate < (startOfWeek + section.days).endOfDay
        }).count
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if self.eventsDict[(startOfWeek + indexPath.section.days).stringFromFormat("yyyy-MM-dd")] == nil {
            let cell: UICollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: eventCellReuseIdentifier,
                for: indexPath)
            if let eventCell: EventCell = cell as? EventCell {
                let training: Training = trainings.filter({ (t) -> Bool in
                    startOfWeek + (indexPath as NSIndexPath).section.days <= t.startDate
                        && t.startDate < (startOfWeek + (indexPath as NSIndexPath).section.days).endOfDay
                })[(indexPath as NSIndexPath).row]
                eventCell.training(training)
            }
            return cell
        } else {
            let cell: UICollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: holidayCellReuseIdentifier,
                for: indexPath)
            cell.backgroundColor = UIColor.lightGray
            return cell
        }
    }

    override func collectionView(_ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView {

        var view: UICollectionReusableView = UICollectionReusableView()

        if let collectView: UICollectionView = self.collectionView {
            if kind == MSCollectionElementKindDayColumnHeader {
                let cell: UICollectionReusableView = collectView
                    .dequeueReusableSupplementaryView(
                        ofKind: kind,
                        withReuseIdentifier: dayColumnHeaderReuseIdentifier,
                        for: indexPath
                )
                if let dayColumnHeader: DayColumnHeader = cell as? DayColumnHeader {
                    let day: Date = self.collectionViewCalendarLayout
                        .dateForDayColumnHeader(at: indexPath)
                    let currentDay: Date = self.currentTimeComponents(
                        for: collectView,
                        layout: self.collectionViewCalendarLayout
                    )
                    let startOfDay: Date = Calendar.current
                        .startOfDay(for: day)
                    let startOfCurrentDay: Date = Calendar.current
                        .startOfDay(for: currentDay)
                    dayColumnHeader.day(day)
                    dayColumnHeader.currentDay(startOfDay == startOfCurrentDay)
                    dayColumnHeader.holidayDay(self.eventsDict[(startOfWeek + indexPath.section.days)
                        .stringFromFormat("yyyy-MM-dd")] != nil)
                    view = dayColumnHeader
                }
            } else if kind == MSCollectionElementKindTimeRowHeader {
                let cell: UICollectionReusableView = collectView
                    .dequeueReusableSupplementaryView(
                        ofKind: kind,
                        withReuseIdentifier: timeRowHeaderReuseIdentifier,
                        for: indexPath
                )
                if let timeRowHeader: TimeRowHeader = cell as? TimeRowHeader {
                    timeRowHeader.time(self.collectionViewCalendarLayout
                        .dateForTimeRowHeader(at: indexPath))
                    view = timeRowHeader
                }
            }
        }
        return view
    }

    // MARK: - MSCollectionViewCalendarLayout
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewCalendarLayout: MSCollectionViewCalendarLayout,
               dayForSection section: Int) -> Date {
        return startOfWeek + section.days
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewCalendarLayout: MSCollectionViewCalendarLayout,
               startTimeForItemAt indexPath: IndexPath) -> Date {
        let isHoliday = self.eventsDict[(startOfWeek + indexPath.section.days).stringFromFormat("yyyy-MM-dd")] != nil

        return isHoliday ? (startOfWeek + indexPath.section.days).beginningOfDay :
            trainings.filter({ (t) -> Bool in
            startOfWeek + (indexPath as NSIndexPath).section.days <= t.startDate!
                && t.startDate! < (startOfWeek + (indexPath as NSIndexPath).section.days).endOfDay
        })[(indexPath as NSIndexPath).row].startDate!
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewCalendarLayout: MSCollectionViewCalendarLayout,
               endTimeForItemAt indexPath: IndexPath) -> Date {
        let isHoliday = self.eventsDict[(startOfWeek + indexPath.section.days).stringFromFormat("yyyy-MM-dd")] != nil

        return isHoliday ? (startOfWeek + indexPath.section.days).endOfDay :
            trainings.filter({ (t) -> Bool in
            startOfWeek + (indexPath as NSIndexPath).section.days <= t.startDate!
                && t.startDate! < (startOfWeek + (indexPath as NSIndexPath).section.days).endOfDay
        })[(indexPath as NSIndexPath).row].endDate!
    }

    func currentTimeComponents(
        for collectionView: UICollectionView,
        layout collectionViewCalendarLayout: MSCollectionViewCalendarLayout) -> Date {
        return Date()
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath) {

        if let cell: EventCell =
            collectionView.cellForItem(at: indexPath) as? EventCell,
            let training = cell.training,
            self.eventsDict[(startOfWeek + indexPath.section.days).stringFromFormat("yyyy-MM-dd")] == nil {

            if adminView {
                let cancelingVC = CancelingViewController()
                cancelingVC.modalTransitionStyle = .crossDissolve
                cancelingVC.training = Training(training: training)
                self.navigationController?.pushViewController(cancelingVC, animated: true)
            } else {
                let signingVC = SigningViewController()
                signingVC.modalTransitionStyle = .crossDissolve
                signingVC.training = Training(training: training)
                signingVC.signedTrainingsCount = trainings.filter({$0.isParticipant == true}).count
                self.navigationController?.pushViewController(signingVC, animated: true)
            }
        }
    }
}
