//
//  LoginService.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Pantry

public let dateFormatter = DateFormatter()

class TrainingsService: BaseService {

    fileprivate static func endpoint() -> String { return "/training" }
    fileprivate let trainingsURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
        + "/date/"
    fileprivate let trainingsFilteredURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
        + "/fromDateFiltered/"
    fileprivate let signupForTrainingURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
        + "/sign_up/"
    fileprivate let unsubscribeForTrainingURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
        + "/unsubscribe/"
    fileprivate let deleteTrainingURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
    fileprivate let cancelTrainingURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
        + "/cancel/"
    fileprivate let generateTrainingURLString: String = BaseService.serverURL()
        + TrainingsService.endpoint()
        + "/generate_courses/date/"
    
    func trainingsForWeek(_ startDate: Date,
                           completion: @escaping (([Training]?, NSError?)) -> Void) {
        if let user: User = Pantry.unpack("lastUser"), let token = user.token {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            Alamofire
                .request(trainingsURLString + "\(dateFormatter.string(from: startDate))",
                    method: .get,
                    headers: ["X-AUTH-TOKEN": token])
                .responseArray { (response: DataResponse<[Training]>) in
                    completion((response.result.value, response.result.error as NSError?))
            }
        }
    }
    
    func trainingsFromDate(_ startDate: Date, days: Int,
                          completion: @escaping (([Training]?, NSError?)) -> Void) {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let urlString = trainingsFilteredURLString + "\(dateFormatter.string(from: startDate))/days/" + "\(days)"
        if let user: User = Pantry.unpack("lastUser"), let token = user.token {
            Alamofire
                .request(urlString,
                    method: .get,
                    headers: ["X-AUTH-TOKEN": token])
                .responseArray { (response: DataResponse<[Training]>) in
                    completion((response.result.value, response.result.error as NSError?))
            }
        }
    }


    func signupForTraining(_ user: User, training: Training,
                           completion: @escaping ((String?, NSError?)) -> Void) {
        if let token = user.token, let trainingId = training.identifier {
            Alamofire
                .request(signupForTrainingURLString + "\(trainingId)",
                    method: .get,
                    headers: ["X-AUTH-TOKEN": token])
                .responseString { (response: DataResponse<String>) in
                    completion((response.result.value, response.result.error as NSError?))
            }
        }
    }

    func unsubscribeFromTraining(_ user: User, training: Training,
                           completion: @escaping ((String?, NSError?)) -> Void) {
        if let token = user.token, let trainingId = training.identifier {
            Alamofire
                .request(unsubscribeForTrainingURLString + "\(trainingId)",
                    method: .get,
                    headers: ["X-AUTH-TOKEN": token])
                .responseString { (response: DataResponse<String>) in
                    completion((response.result.value, response.result.error as NSError?))
            }
        }
    }

    func trainingForId(_ startDate: Date, training: Training,
                          completion: @escaping ((Training?, NSError?)) -> Void) {
        if let user: User = Pantry.unpack("lastUser"), let token = user.token,
            let trainingId = training.identifier {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            Alamofire
                .request(trainingsURLString + "\(dateFormatter.string(from: startDate))",
                    method: .get,
                    headers: ["X-AUTH-TOKEN": token])
                .responseArray { (response: DataResponse<[Training]>) in
                    let selectedTraining: Training? = response.result.value?
                        .filter({$0.identifier == trainingId}).first
                    completion((selectedTraining, response.result.error as NSError?))
            }
        }
    }
    
    func saveTraining(_ user: User, training: Training,
                        completion: @escaping ((Training?, NSError?, Data?)) -> Void) {
        if let token = user.token,
            let date = training.toDictionary()["date"],
            let identifier: Int = training.schematic?.identifier{
            
            let schematicDict: [String: Any] = [ "id": identifier ]
            let trainingDict: [String: AnyObject] =
                [
                    "date": date,
                    "schematic" : schematicDict as AnyObject
                ]
            
            Alamofire
                .request(BaseService.serverURL() + TrainingsService.endpoint(),
                    method: .post,
                    parameters: trainingDict,
                    encoding: JSONEncoding.default,
                    headers: ["X-AUTH-TOKEN": token])
                .responseObject { (response: DataResponse<Training>) in
                    completion((response.result.value, response.result.error as NSError?, response.data))
            }
        }
    }

    func deleteTraining(_ user: User, training: Training,
                                  completion: @escaping ((String?, NSError?)) -> Void) {
        if let token = user.token, let trainingId = training.identifier {
            Alamofire
                .request(deleteTrainingURLString + "/\(trainingId)",
                    method: .delete,
                    headers: ["X-AUTH-TOKEN": token])
                .responseString { (response: DataResponse<String>) in
                    completion((response.result.value, response.result.error as NSError?))
            }
        }
    }

    func cancelTraining(_ user: User, training: Training,
                           completion: @escaping ((String?, NSError?)) -> Void) {
        if let token = user.token, let trainingId = training.identifier {
            Alamofire
                .request(cancelTrainingURLString + "\(trainingId)",
                    method: .get,
                    headers: ["X-AUTH-TOKEN": token])
                .responseString { (response: DataResponse<String>) in
                    completion((response.result.value, response.result.error as NSError?))
            }
        }
    }
    func generateTrainings(_ user: User, training: Training, ignoreEvents: Bool,
                        completion: @escaping ((String?, NSError?)) -> Void) {
        if let token = user.token,
            let date = training.startDate,
            let schematicId = training.schematic?.identifier {
            
            do {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"

                let requestUrl = generateTrainingURLString + "\(dateFormatter.string(from: date))" +
                    "/schematic/" + "\(schematicId)" + "/" + (ignoreEvents ? "true" : "false")
                
                var request = URLRequest(url: URL(string: requestUrl)!)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(token, forHTTPHeaderField: "X-AUTH-TOKEN")
                
                if let users = training.users, users.count > 0 {
                    let usersArray: [[String: AnyObject]] = users.map({
                        (user) -> [String: AnyObject] in
                        user.toDictionary(true)
                    })
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: usersArray,
                                                              options: .init(rawValue: 0))
                    request.httpBody = jsonData
                } else {
                    let jsonData = "[]".data(using: .utf8, allowLossyConversion: false)
                    request.httpBody = jsonData
                }
                
                Alamofire
                    .request(request)
                    .responseString { (response: DataResponse<String>) in
                        completion((response.result.value, response.result.error as NSError?))
                }
            } catch let error as NSError {
                completion((nil, error))
            }
        }
    }
}
