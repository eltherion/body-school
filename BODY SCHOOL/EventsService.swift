//
//  EventsService.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Pantry

class EventsService: BaseService {

    fileprivate static func endpoint() -> String { return "/events" }
    fileprivate let eventsURLString: String = BaseService.serverURL()
        + EventsService.endpoint()
    fileprivate let eventsForWeekURLString: String = BaseService.serverURL()
        + EventsService.endpoint()
        + "/date/"

    func fetchEventsList(_ token: String,
                       completion: @escaping (([Event]?, NSError?) -> Void)) {
        Alamofire
            .request(eventsURLString,
                method: .get,
                headers: ["X-AUTH-TOKEN": token])
            .responseArray { (response: DataResponse<[Event]>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }
    
    func fetchEventsForWeek(_ dateString: String, _ token: String,
                         completion: @escaping (([Event]?, NSError?) -> Void)) {
        Alamofire
            .request(eventsForWeekURLString + dateString,
                     method: .get,
                     headers: ["X-AUTH-TOKEN": token])
            .responseArray { (response: DataResponse<[Event]>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }

    func saveEvent(_ event: Event, token: String,
                  completion: @escaping ((Event?, NSError?) -> Void)) {
        Alamofire
            .request(eventsURLString,
                method: .post,
                parameters: event.toDictionary(),
                encoding: JSONEncoding.default,
                headers: ["X-AUTH-TOKEN": token])
            .responseObject { (response: DataResponse<Event>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }

    func deleteEventWithIdentifier(_ identifier: Int, token: String,
                                  completion: @escaping ((String?, NSError?) -> Void)) {
        Alamofire
            .request(eventsURLString+"/\(identifier)",
                method: .delete,
                headers: ["X-AUTH-TOKEN": token])
            .responseString { (response: DataResponse<String>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }


}
