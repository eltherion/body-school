//
//  RYRCalendarStyle.swift
//  RYRCalendar
//
//  Created by Miquel, Aram on 07/06/2016.
//  Copyright © 2016 Ryanair. All rights reserved.
//

import UIKit

public struct RYRCalendarStyle {
   
   // CollectionView properties
   public var monthHeaderHeight: Double = 30
   public var monthHeaderStyle = RYRMonthHeaderStyle(font: UIFont(name: "Arial", size: 16)!, textColor: UIColor(red:7.0/255.0, green:53.0/255.0, blue:147.0/255.0, alpha:1.0), backgroundColor: UIColor(red:246.0/255.0, green:248.0/255.0, blue:255.0/255.0, alpha:1.0))
   public var calendarHeaderStyle = RYRCalendarHeaderStyle(workDayFont: UIFont(name: "Arial", size: 16)!, workDayTextColor: UIColor(red:7.0/255.0, green:53.0/255.0, blue:147.0/255.0, alpha:1.0), weekendDayFont: UIFont(name: "Arial", size: 16)!, weekendDayTextColor: UIColor(red:7.0/255.0, green:53.0/255.0, blue:147.0/255.0, alpha:1.0), backgroundColor: UIColor(red:246.0/255.0, green:248.0/255.0, blue:255.0/255.0, alpha:1.0))
   
   // Cell styles
   public var cellStyleEnabled = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: nil, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.black)
   public var cellStyleDisabled = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: nil, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.lightGray)
   public var cellStyleToday = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: UIImage.loadImageInBundle("current_day_highlight"), backgroundImageContentMode: .scaleToFill, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.black)
   public var cellStyleEmpty = RYREmptyCellStyle(backgroundColor: UIColor.white)
   
   // Single selection cell style
   public var cellStyleSelected = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: UIImage.loadImageInBundle("one_way_highlight"), backgroundImageContentMode: .scaleToFill, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.white)
   public var cellStyleSelectedMultiple = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: UIImage.loadImageInBundle("same_day_highlight"), backgroundImageContentMode: .scaleToFill, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.white)
   
   // Multiple selection cell styles
   public var cellStyleFirstSelected = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: UIImage.loadImageInBundle("closed_return_highlight_left"), backgroundImageContentMode: .scaleToFill, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.white)
   public var cellStyleLastSelected = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: UIImage.loadImageInBundle("closed_return_highlight_right"), backgroundImageContentMode: .scaleToFill, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.white)
   public var cellStyleMiddleSelected = RYRDayCellStyle(backgroundColor: UIColor.white, backgroundImage: UIImage.loadImageInBundle("highlight_travel_day"), backgroundImageContentMode: .scaleToFill, textFont: UIFont(name: "Arial", size: 16)!, textColor: UIColor.black)
}

extension UIImage {
   class func loadImageInBundle(_ named: String) -> UIImage? {
      let podBundle = Bundle(for: RYRCalendar.classForCoder())
      if let bundleURL = podBundle.url(forResource: "RYRCalendar", withExtension: "bundle"), let bundle = Bundle(url: bundleURL) {
         return UIImage(named: named, in: bundle, compatibleWith: nil)
      }
      return nil
   }
}
