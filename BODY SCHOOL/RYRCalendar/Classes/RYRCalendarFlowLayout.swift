//
//  RYRCalendarFlowLayout.swift
//  RYRCalendar
//
//  Created by Miquel, Aram on 03/06/2016.
//  Copyright © 2016 Ryanair. All rights reserved.
//

import UIKit

class RYRCalendarFlowLayout: UICollectionViewFlowLayout {
   
   override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
      
      guard var superAttributes = super.layoutAttributesForElements(in: rect) else {
         return super.layoutAttributesForElements(in: rect)
      }
      
      let contentOffset = collectionView!.contentOffset
      let missingSections = NSMutableIndexSet()
      
      for layoutAttributes in superAttributes where (layoutAttributes.representedElementCategory == .cell && layoutAttributes.representedElementKind != UICollectionElementKindSectionHeader) {
         missingSections.add((layoutAttributes.indexPath as NSIndexPath).section)
      }
      
      missingSections.enumerate( { idx, stop in
         let indexPath = IndexPath(item: 0, section: idx)
         if let layoutAttributes = self.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath) {
            superAttributes.append(layoutAttributes)
         }
      })
      
      for layoutAttributes: UICollectionViewLayoutAttributes in superAttributes {
         
         if let representedElementKind = layoutAttributes.representedElementKind , representedElementKind == UICollectionElementKindSectionHeader {
            let section = (layoutAttributes.indexPath as NSIndexPath).section
            let numberOfItemsInSection = collectionView!.numberOfItems(inSection: section)
            
            if numberOfItemsInSection > 0 {
               let firstCellIndexPath = IndexPath(item: 0, section: section)
               let lastCellIndexPath = IndexPath(item: max(0, (numberOfItemsInSection - 1)), section: section)
               
               var firstCellAttributes:UICollectionViewLayoutAttributes
               var lastCellAttributes:UICollectionViewLayoutAttributes
               
               firstCellAttributes = self.layoutAttributesForItem(at: firstCellIndexPath)!
               lastCellAttributes = self.layoutAttributesForItem(at: lastCellIndexPath)!
               
               let headerHeight = layoutAttributes.frame.height
               var origin = layoutAttributes.frame.origin
               
               origin.y = min(max(contentOffset.y, (firstCellAttributes.frame.minY - headerHeight)), (lastCellAttributes.frame.maxY - headerHeight));
               
               layoutAttributes.zIndex = 1024;
               layoutAttributes.frame = CGRect(origin: origin, size: layoutAttributes.frame.size)
            }
         }
      }
      
      return NSArray(array: superAttributes) as? [UICollectionViewLayoutAttributes]
   }
   
   override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
      return true
   }
}
