//
//  NSDateExtensions.swift
//  RYRCalendar
//
//  Created by Miquel, Aram on 13/06/2016.
//  Copyright © 2016 Ryanair. All rights reserved.
//

import UIKit

internal extension Date {
   func numberOfDaysInCurrentMonth() -> Int {
      return (Calendar.current as NSCalendar).range(of: .day, in: .month, for: self).length
   }
   
   func dateByAddingMonths(_ monthsToAdd: Int) -> Date? {
         return (Calendar.current as NSCalendar).date(byAdding: .month, value: monthsToAdd, to: self, options: [])
    }
   
   func setToFirstDayOfMonth() -> Date? {
      let calendar = Calendar.current
      let components = (calendar as NSCalendar).components([NSCalendar.Unit.year, NSCalendar.Unit.month], from: self)
      return calendar.date(from: components)
   }
   
   func dateByAddingDays(_ daysToAdd: Int) -> Date? {
         return (Calendar.current as NSCalendar).date(byAdding: .day, value: daysToAdd, to: self, options: [])
   }
   
   func isPastDay() -> Bool {
      let calendar = Calendar.current
      let components = (calendar as NSCalendar).components([.year, .day, .month], from: Date())
      return compare(calendar.date(from: components)!) == .orderedAscending
   }
   
   func isToday() -> Bool {
         return  Calendar.current.isDateInToday(self)
    }
   
   func isBetweenDates(_ firstDate: Date?, secondDate: Date?) -> Bool {
      guard let firstDate = firstDate, let secondDate = secondDate else { return false }
      return firstDate.compare(self) == self.compare(secondDate)
   }
   
   func getDayNumberAsString() -> String {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "E"
      return dateFormatter.string(from: self)
   }
   
   func getMonthAsString() -> String {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "MMM"
      return dateFormatter.string(from: self)
   }
   
   func getYearAsString() -> String {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy"
      return dateFormatter.string(from: self)
   }
}
