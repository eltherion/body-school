//
//  RYRCalendarHeaderReusableView.swift
//  RYRCalendar
//
//  Created by Miquel, Aram on 02/06/2016.
//  Copyright © 2016 Ryanair. All rights reserved.
//

import UIKit

class RYRMonthHeaderReusableView: UICollectionReusableView {
   
   class var identifier: String { get { return "RYRCalendarHeaderReusableViewIndentifier" } }
   
   var date: Date? { didSet { updateDate(date!) } }
   var style: RYRMonthHeaderStyle? { didSet { updateStyle(style!) } }
   
   fileprivate var dateLabel: UILabel!
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      setup()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setup()
   }
   
   fileprivate func setup() {
      backgroundColor = UIColor.yellow
      
      dateLabel = UILabel(frame: CGRect.zero)
      addSubview(dateLabel)
      
      dateLabel.translatesAutoresizingMaskIntoConstraints = false
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-12-[dateLabel]-0-|", options: [], metrics: nil, views: ["dateLabel": dateLabel]))
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[dateLabel]-0-|", options: [], metrics: nil, views: ["dateLabel": dateLabel]))
   }
   
   fileprivate func updateDate(_ date: Date) {
      dateLabel.text = "\(date.getMonthAsString()) \(date.getYearAsString())"
   }
   
   fileprivate func updateStyle(_ style: RYRMonthHeaderStyle) {
      backgroundColor = style.backgroundColor
      dateLabel.font = style.font
      dateLabel.textColor = style.textColor
   }
}
