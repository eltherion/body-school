//
//  RYRCalendar.swift
//  RYRCalendar
//
//  Created by Miquel, Aram on 01/06/2016.
//  Copyright © 2016 Ryanair. All rights reserved.
//

import UIKit

public protocol RYRCalendarDelegate: class {
   func calendarDidSelectDate(_ calendar: RYRCalendar, selectedDate: Date)
   func calendarDidSelectMultipleDate(_ calendar: RYRCalendar, selectedStartDate startDate: Date, endDate: Date)
   func isDateAvailableToSelect(_ date: Date) -> Bool
   func calendarDidScrollToMonth(_ calendar: RYRCalendar, monthDate: Date)
}

extension RYRCalendarDelegate {
   func isDateAvailableToSelect(_ date: Date) -> Bool {
      return true
   }
}

public enum RYRCalendarSelectionType: Int {
   case none, single, multiple
}

open class RYRCalendar: UIView {
   
   // PUBLIC (API)
   open var style: RYRCalendarStyle = RYRCalendarStyle() { didSet { update() } }
   open weak var delegate: RYRCalendarDelegate?
   open var selectionType: RYRCalendarSelectionType = .none
   open var totalMonthsFromNow: Int = 1
   
   // Date configuration
   open var baseDate: Date = Date()
   
   // PRIVATE
   fileprivate var singleSelectionIndexPath: IndexPath?
   fileprivate var multipleSelectionIndexPaths = [IndexPath]()
   
   fileprivate var headerView: RYRCalendarHeaderView!
   fileprivate var collectionView: UICollectionView!
   fileprivate var collectionViewLayout: UICollectionViewFlowLayout!
   
   fileprivate var cellSize: CGFloat {
      get {
         return CGFloat(Int(frame.size.width / 7))
      }
   }
   
   // MARK: Initializers
   init() {
      super.init(frame: CGRect.zero)
      setup()
   }
   
   override init(frame: CGRect) {
      super.init(frame: frame)
      setup()
   }
   
   required public init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setup()
   }
   
   override open func didMoveToSuperview() {
      self.collectionView.frame = superview!.frame
   }
   
   // MARK: Public
   open func update() {
      headerView.style = style.calendarHeaderStyle
      collectionView.reloadData()
   }
   
   // MARK: Private
   fileprivate func setup() {
      
      headerView = RYRCalendarHeaderView()
      headerView.style = style.calendarHeaderStyle
      addSubview(headerView)
      
      
      if #available(iOS 9.0, *) {
         collectionViewLayout = UICollectionViewFlowLayout()
         collectionViewLayout.sectionHeadersPinToVisibleBounds = true
      } else {
         collectionViewLayout = RYRCalendarFlowLayout()
      }
      
      collectionViewLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
      collectionView = UICollectionView(frame: frame, collectionViewLayout: collectionViewLayout)
      addSubview(collectionView)
      
      headerView.translatesAutoresizingMaskIntoConstraints = false
      collectionView.translatesAutoresizingMaskIntoConstraints = false
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[collectionView]-0-|", options: [], metrics: nil, views: ["collectionView": collectionView]))
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[headerView]-0-|", options: [], metrics: nil, views: ["headerView": headerView]))
      addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[headerView(30)][collectionView]-0-|",
        options: [], metrics: nil, views: ["collectionView": collectionView, "headerView": headerView]))
      
      collectionView.dataSource = self
      collectionView.delegate = self
      collectionView.backgroundColor = UIColor.clear
      collectionView.register(RYRDayCell.self, forCellWithReuseIdentifier: RYRDayCell.cellIndentifier)
      collectionView.register(RYREmptyDayCell.self, forCellWithReuseIdentifier: RYREmptyDayCell.cellIndentifier)
      collectionView.register(RYRMonthHeaderReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: RYRMonthHeaderReusableView.identifier)
      
      update()
   }
   
   internal func getBlankSpaces(_ fromDate: Date?) -> Int {
      if let firstDayOfMonth = fromDate?.setToFirstDayOfMonth() {
         
         let components = (Calendar.current as NSCalendar).components([.weekday], from: firstDayOfMonth)
         
         let blankSpaces = components.weekday! - 2 // this '2' is because components.Weekday starts on Saturday, and starts with 1 (not 0)
         
         return blankSpaces
      }
      
      return 0
   }
   
   fileprivate func selectSingleIndexPath(_ indexPath: IndexPath) {
      
      if let previousSelectedIndexPath = singleSelectionIndexPath {
         unselectIndexPath(previousSelectedIndexPath)
      }
      
      if let date = dateFromIndexPath(indexPath) {
         if delegate?.isDateAvailableToSelect(date) ?? false {
            
            singleSelectionIndexPath = indexPath
            collectionView.reloadItems(at: [indexPath])
            
            delegate?.calendarDidSelectDate(self, selectedDate: date)
         }
      }
   }
   
   fileprivate func selectMultipleIndexPath(_ indexPath: IndexPath) {
      guard let newDateSelection = dateFromIndexPath(indexPath) else { print("Error converting indexPath from selected date"); return }
      
      if let firstSelectedIndex = multipleSelectionIndexPaths.first, let firstDateSelection = dateFromIndexPath(firstSelectedIndex) {
         
         if multipleSelectionIndexPaths.count > 1 {
            // Contains two previously selected dates
            
            // We empty the selection, and select the newDate
            multipleSelectionIndexPaths = []
            multipleSelectionIndexPaths.append(indexPath)
            
            if let date = dateFromIndexPath(indexPath) {
               delegate?.calendarDidSelectDate(self, selectedDate: date)
            }
         } else {
            
            // Contains one previously selected date
            let order = firstDateSelection.compare(newDateSelection)
            switch order {
            case .orderedAscending:
               // If the firstDateSelection is before the newDateSelection, newDateSelection will become the second selected date
               multipleSelectionIndexPaths.append(indexPath)
            case .orderedDescending:
               // If the firstDateSelection is after the newDateSelection, multipleSelectionIndexPaths will be emptied and then newDateSelection added
               multipleSelectionIndexPaths = []
               multipleSelectionIndexPaths.append(indexPath)
               
               if let date = dateFromIndexPath(indexPath) {
                  delegate?.calendarDidSelectDate(self, selectedDate: date)
               }
               
            case .orderedSame:
               // If the firstDateSelection is same as newDateSelection, multipleSelectionIndexPaths will be emptied
               multipleSelectionIndexPaths.append(indexPath)
            }
            
            if multipleSelectionIndexPaths.count > 1 {
               if let startDate = dateFromIndexPath(multipleSelectionIndexPaths.first), let endDate = dateFromIndexPath(multipleSelectionIndexPaths.last) {
                  delegate?.calendarDidSelectMultipleDate(self, selectedStartDate: startDate, endDate: endDate)
               }
            }
         }
      } else {
         // Does not contain any date
         multipleSelectionIndexPaths.append(indexPath)
         
         if let date = dateFromIndexPath(indexPath) {
            delegate?.calendarDidSelectDate(self, selectedDate: date)
         }
      }
      
      collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems)
   }
   
   fileprivate func unselectIndexPath(_ indexPath: IndexPath) {
      singleSelectionIndexPath = nil
      collectionView.reloadItems(at: [indexPath])
   }
   
   fileprivate func dateFromIndexPath(_ indexPath: IndexPath?) -> Date? {
      guard let indexPath = indexPath else { return nil }
      
      let monthDate = baseDate.dateByAddingMonths((indexPath as NSIndexPath).section)?.setToFirstDayOfMonth()
      let blankSpaces = getBlankSpaces(monthDate)
      return monthDate?.dateByAddingDays((indexPath as NSIndexPath).row - blankSpaces)
   }
}

// MARK: CollectionView DataSource
extension RYRCalendar: UICollectionViewDataSource {
   public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      let dateForSection = baseDate.dateByAddingMonths(section)
      var numberOfItems = dateForSection?.numberOfDaysInCurrentMonth() ?? 0
      numberOfItems += getBlankSpaces(dateForSection) 
      
      return numberOfItems
   }
   
   public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let dateForSection = baseDate.dateByAddingMonths((indexPath as NSIndexPath).section)?.setToFirstDayOfMonth()
      let blankSpaces = getBlankSpaces(dateForSection)
      
      if (indexPath as NSIndexPath).row >= blankSpaces {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RYRDayCell.cellIndentifier, for: indexPath) as! RYRDayCell
         cell.dayNumber = (indexPath as NSIndexPath).row - blankSpaces + 1

         if let rowDate = dateForSection?.dateByAddingDays((indexPath as NSIndexPath).row - blankSpaces) {
            if rowDate.isPastDay() {
               cell.style = style.cellStyleDisabled
            } else if indexPath == singleSelectionIndexPath {
               cell.style = style.cellStyleSelected
            } else if multipleSelectionIndexPaths.contains(indexPath) {
               if multipleSelectionIndexPaths.count > 1 && multipleSelectionIndexPaths.first == multipleSelectionIndexPaths.last {
                  cell.style = style.cellStyleSelectedMultiple
               } else if multipleSelectionIndexPaths.first == indexPath {
                  cell.style = style.cellStyleFirstSelected
               } else if multipleSelectionIndexPaths.last == indexPath {
                  cell.style = style.cellStyleLastSelected
               }
            } else if rowDate.isBetweenDates(dateFromIndexPath(multipleSelectionIndexPaths.first), secondDate: dateFromIndexPath(multipleSelectionIndexPaths.last)) {
               cell.style = style.cellStyleMiddleSelected
            }else if rowDate.isToday() {
               cell.style = style.cellStyleToday
            } else if delegate?.isDateAvailableToSelect(rowDate) ?? true {
               cell.style = style.cellStyleEnabled
            } else {
               cell.style = style.cellStyleDisabled
            }
         }
         return cell
      } else {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RYREmptyDayCell.cellIndentifier, for: indexPath) as! RYREmptyDayCell
         
         return cell
      }
   }

   public func numberOfSections(in collectionView: UICollectionView) -> Int {
      return totalMonthsFromNow
   }
}

// MARK: CollectionView Delegate
extension RYRCalendar: UICollectionViewDelegate {
   public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
      switch selectionType {
      case .none:
         return
      case .single:
         selectSingleIndexPath(indexPath)
      case .multiple:
         selectMultipleIndexPath(indexPath)
      }
   }
   
   public func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
      if let view = view as? RYRMonthHeaderReusableView {
         delegate?.calendarDidScrollToMonth(self, monthDate: view.date! as Date)
      }
   }
}

// MARK: FlowLayout Delegate
extension RYRCalendar: UICollectionViewDelegateFlowLayout {
   public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      return CGSize(width: cellSize, height: cellSize)
   }
   
   @objc(collectionView:viewForSupplementaryElementOfKind:atIndexPath:) public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
      if kind == UICollectionElementKindSectionHeader {
         let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: RYRMonthHeaderReusableView.identifier, for: indexPath) as! RYRMonthHeaderReusableView
         view.date = baseDate.setToFirstDayOfMonth()!.dateByAddingMonths((indexPath as NSIndexPath).section)
         view.style = style.monthHeaderStyle
         return view
      }
      
      return UICollectionReusableView()
   }
   
   public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
      return CGSize(width: collectionView.frame.width, height: CGFloat(style.monthHeaderHeight))
   }
   
   public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return 0
   }
   
   public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return 0
   }
   
   public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      let lateralSpacing = (frame.width - (cellSize * 7)) / 2
      return UIEdgeInsets(top: 0, left: lateralSpacing, bottom: 0, right: lateralSpacing)
   }
   
}
