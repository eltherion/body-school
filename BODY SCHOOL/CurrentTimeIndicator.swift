//
//  CurrentTimeIndicator.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit
import Timepiece

class CurrentTimeIndicator: UICollectionReusableView {

    var time: UILabel
    var minuteTimer: Timer
    let dateFormatter: DateFormatter = DateFormatter()

    override init(frame: CGRect) {
        self.time = UILabel()
        let calendar: Calendar = Calendar.current
        let oneMinuteInFuture: Date = Date() + 1.minute
        let components: DateComponents = (calendar as NSCalendar).components([
            NSCalendar.Unit.year,
            NSCalendar.Unit.month,
            NSCalendar.Unit.day,
            NSCalendar.Unit.hour,
            NSCalendar.Unit.minute], from: oneMinuteInFuture)
        self.minuteTimer = Timer()

        super.init(frame: frame)

        if let nextMinuteBoundary: Date = calendar.date(from: components) {
            self.minuteTimer = Timer(
                fireAt: nextMinuteBoundary,
                interval: 60,
                target: self,
                selector: #selector(CurrentTimeIndicator.minuteTick(_:)),
                userInfo: nil,
                repeats: true
            )
        }

        self.backgroundColor = UIColor.white
        self.time.font = UIFont.boldSystemFont(ofSize: 10.0)
        self.time.textColor = UIColor.colorWithHexString("fd3935")
        self.addSubview(self.time)
        self.time.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.centerY.equalTo()(self.mas_centerY)
            let _ = make?.right.equalTo()(self.mas_right)?.offset()(-5.0)
        }

        dateFormatter.dateFormat = "HH:mm"
        RunLoop.current.add(self.minuteTimer, forMode: RunLoopMode.defaultRunLoopMode)
        self.updateTime()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - CurrentTimeIndicator
    func minuteTick(_ sender: AnyObject) {
        self.updateTime()
    }

    func updateTime() {
        self.time.text = dateFormatter.string(from: Date())
        self.time.sizeToFit()
    }
}
