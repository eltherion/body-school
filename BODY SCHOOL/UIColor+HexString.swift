//
//  UIColor+HexString.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var length: Int {
        return characters.count
    }
}

extension UIColor {

    class func colorComponentFrom(_ string: String, start: Int, length: Int) -> CGFloat {

        let substr = NSString(string: string).substring(
            with: NSRange(location: start, length: length)
        )
        let substring: String = "\(substr)"
        let fullHex: String = length == 2 ? substring : "\(substring)\(substring)"
        if let hexComponent: Int = Int(fullHex, radix: 16) {
            return CGFloat(hexComponent)/255.0
        } else {
            return 0
        }
    }

    class func colorWithHexString(_ hexString: String) -> UIColor {
        let colorString: String = hexString
            .replacingOccurrences(of: "#", with: "").uppercased()
        var alpha: CGFloat
        var red: CGFloat
        var blue: CGFloat
        var green: CGFloat
        switch colorString.length {
        case 3:

            // #RGB
            alpha = 1.0
            red = self.colorComponentFrom(colorString, start: 0, length: 1)
            green = self.colorComponentFrom(colorString, start: 1, length: 1)
            blue = self.colorComponentFrom(colorString, start: 2, length: 1)
            break
        case 4:

            // #ARGB
            alpha = self.colorComponentFrom(colorString, start: 0, length: 1)
            red = self.colorComponentFrom(colorString, start: 1, length: 1)
            green = self.colorComponentFrom(colorString, start: 2, length: 1)
            blue = self.colorComponentFrom(colorString, start: 3, length: 1)
            break
        case 6:

            // #RRGGBB
            alpha = 1.0
            red = self.colorComponentFrom(colorString, start: 0, length: 2)
            green = self.colorComponentFrom(colorString, start: 2, length: 2)
            blue = self.colorComponentFrom(colorString, start: 4, length: 2)
            break
        case 8:

            // #AARRGGBB
            alpha = self.colorComponentFrom(colorString, start: 0, length: 2)
            red = self.colorComponentFrom(colorString, start: 2, length: 2)
            green = self.colorComponentFrom(colorString, start: 4, length: 2)
            blue = self.colorComponentFrom(colorString, start: 6, length: 2)
            break
        default:

            return UIColor.clear
        }
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }

}
