//
//  LogoPresenter.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/6/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit

@objc protocol LogoPresenter {
    var logoView: UIImageView? {get set}
}

extension LogoPresenter {


    fileprivate func verticalFrame() -> CGRect { return CGRect(x: 0, y: 0, width: 82, height: 44)}
    fileprivate func horizontalFrame() -> CGRect { return CGRect(x: 0, y: 0, width: 60, height: 32)}

    func setLogoFrame(_ navigationItem: UINavigationItem) {

        if navigationItem.rightBarButtonItem == nil {
            self.logoView = UIImageView(
                image: UIImage(named: "logo_navigation_bar.png"))
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                customView: self.logoView!)
        }

        if UIInterfaceOrientationIsLandscape(
            UIApplication.shared.statusBarOrientation
        ) {
            logoView?.frame = horizontalFrame()
        } else {
            logoView?.frame = verticalFrame()
        }
    }

    func prefersStatusBarHidden() -> Bool {
        return true
    }
}
