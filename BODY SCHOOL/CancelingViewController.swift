//
//  CancelingViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import Pantry
import Timepiece

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class CancelingViewController: FormViewController, LogoPresenter,
    UITextViewDelegate, UIAlertViewDelegate {

    fileprivate let trainingService = TrainingsService()
    fileprivate let settingsService = SettingsService()
    fileprivate let rowHeight: CGFloat = 150
    fileprivate let trainingSchemeView = EventCell()
    fileprivate let dateFormatter = DateFormatter()

    var logoView: UIImageView?
    var training: Training?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setLogoFrame(navigationItem)

        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Cancel", comment: ""),
            style: .plain,
            target: self,
            action: #selector(cancelButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")

        dateFormatter.dateFormat = "HH:mm"
        setupView()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    func setupView() {
        form +++ Section(NSLocalizedString("Description:", comment: ""))

            <<< TextAreaRow() {
                if let descr = self.training?.schematic?.descr {
                    $0.cell.textView.insertText(descr)
                }
                $0.textAreaHeight = .dynamic(initialTextViewHeight: self.rowHeight)
                $0.cell.textView.isEditable = false
                $0.cell.textView.isSelectable = false
                $0.cell.textView.delegate = self
            }

            +++ Section()

            <<< SchemeRow().cellSetup({ (cell, row) in
                self.setupSchemeRow(cell)
            })
    }

    // MARK: - text view delegate methods
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }

    // MARK: - table/form view delegate methods
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 {
            return max(rowHeight, self.view.frame.height - rowHeight - 150)
        } else {
            return rowHeight
        }
    }

    func tableView(_ tableView: UITableView,
                   shouldHighlightRowAtIndexPath indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView,
                            titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? NSLocalizedString("Description:", comment: "") : nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.text = NSLocalizedString("Description:", comment: "")
        }
    }

    // MARK: - custom scheme row initialization
    fileprivate func setupSchemeRow(_ cell: UIView) {
        if let t = self.training {
            self.trainingSchemeView.training(t)
            cell.addSubview(self.trainingSchemeView)

            self.trainingSchemeView.mas_makeConstraints({ (make) in
                let _ = make?.centerX.equalTo()(cell.mas_centerX)
                let _ = make?.top.equalTo()(cell.mas_top)?.offset()(8)
                let _ = make?.width.equalTo()(254)
                let _ = make?.height.equalTo()(70)
            })

            self.setupRowButtons(cell)
        }
    }

    fileprivate func setupRowButtons(_ cell: UIView) {
        let deleteButton = UIButton()
        deleteButton.backgroundColor = UIColor.red.withAlphaComponent(0.8)
        deleteButton.setTitleColor(UIColor.white, for: UIControlState())
        deleteButton.setTitle(NSLocalizedString("Delete training", comment: ""),
                            for: UIControlState())
        deleteButton.addTarget(self,
                             action: #selector(changeTrainingState),
                             for: .touchUpInside)

        let cancelButton = UIButton()
        cancelButton.tag = 1
        cancelButton.backgroundColor = UIColor.orange
        cancelButton.setTitleColor(UIColor.white, for: UIControlState())
        cancelButton.titleLabel?.numberOfLines = 0
        cancelButton.titleLabel?.lineBreakMode = .byWordWrapping
        cancelButton.titleLabel?.textAlignment = .center
        cancelButton.setTitle(NSLocalizedString("Cancel training", comment: ""),
                              for: UIControlState())
        cancelButton.addTarget(self,
                               action: #selector(changeTrainingState),
                               for: .touchUpInside)

        cell.addSubview(deleteButton)
        cell.addSubview(cancelButton)

        deleteButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.right.equalTo()(cell.mas_centerX)?.offset()(-4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })

        cancelButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.left.equalTo()(cell.mas_centerX)?.offset()(4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })
    }

    @objc fileprivate func changeTrainingState(_ sender: UIButton) {
        if let training = self.training,
        let startDate = training.startDate?.beginningOfDay {
            trainingService.trainingForId(startDate, training: training,
                completion: { (selectedTraining, error) in
                    if let t = selectedTraining, let user: User = Pantry.unpack("lastUser"),
                        let token = user.token {
                        if sender.tag == 0 {
                            self.deleteTrainingWithConfirmation(user, token: token, training:t)
                        } else {
                            self.cancelTraining(user, token: token, training:t)
                        }
                    } else {
                        self.showError(NSLocalizedString(
                            "No Internet connection. \nCheck connection and try later.",
                            comment: ""))
                    }
            })
        }
    }
    
    fileprivate func deleteTrainingWithConfirmation(_ user: User, token: String, training: Training){
        
        let messageString: String = NSLocalizedString("Do you want to delete training?", comment: "")
        
        let alertVC = UIAlertController(title: NSLocalizedString("Confirmation", comment: ""),
                                        message: messageString,
                                        preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""),
                                         style: .cancel) { (action) in
                                            // ...
        }
        alertVC.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""),
                                     style: .default) { (action) in
            self.deleteTraining(user, token: token, training: training)
        }
        alertVC.addAction(OKAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }

    fileprivate func deleteTraining(_ user: User, token: String, training: Training) {
        settingsService.fetchSettings(token) { (settingsArray, error) in
            if let settings: [Setting] = settingsArray,
            let ioString: String = settings.filter({$0.name == END})
            .first?.value,
            let io: Int = Int(ioString),
            let startDate = training.startDate,
            let trainingName = training.schematic?.schemaName,
            let trainingTime = training.startDate {
                let messageStart = NSLocalizedString("Course ", comment: "")
                    + trainingName + " - " + self.dateFormatter.string(from: trainingTime)
                if training.startDate < Date() {
                    self.showError(NSLocalizedString(
                        "Training has expired. \nRemoval is unavailable.",
                        comment: ""))
                } else if let isCanceled = training.isCanceled , isCanceled {
                    self.showError(NSLocalizedString(
                        "Training can't be removed, because it was canceled.",
                        comment: ""))
                } else if training.usersCount > 0 {
                    self.showError(messageStart + NSLocalizedString(" can't be removed, because it has reservations.",
                                                                    comment: ""))
                } else if startDate - io.hours < Date() {
                    self.showError(NSLocalizedString(
                        "Time to delete training has expired. \nRemoval is unavailable.",
                        comment: ""))
                } else {
                    self.trainingService.deleteTraining(user,
                        training: training, completion: { (responseString, error) in
                            if error == nil {
                                let _ = self.navigationController?.popViewController(animated: true)
                            } else {
                                self.showError((error?.localizedDescription)!)
                            }
                    })
                }
            }
        }
    }
    
    fileprivate func cancelTraining(_ user: User, token: String, training: Training) {

        settingsService.fetchSettings(token) { (settingsArray, error) in
            if let settings: [Setting] = settingsArray,
            let ioString: String = settings.filter({$0.name == END})
                                        .first?.value,
            let io: Int = Int(ioString),
            let startDate = training.startDate {

                if training.startDate < Date() {
                    self.showError(NSLocalizedString(
                        "Training has expired. \nCancellation is unavailable.",
                        comment: ""))
                } else if let isCanceled = training.isCanceled , isCanceled {
                    self.showError(NSLocalizedString(
                        "Training was already canceled. \nCancellation is unavailable.",
                        comment: ""))
                } else if startDate - io.hours < Date() {
                    self.showError(NSLocalizedString(
                        "Time to cancel training has expired. \nCancellation is unavailable.",
                        comment: ""))
                } else {
                    self.trainingService.cancelTraining(user, training: training,
                        completion: { (responseString, error) in
                            if error == nil {
                                let _ = self.navigationController?.popViewController(animated: true)
                            } else {
                                self.showError((error?.localizedDescription)!)
                            }
                    })
                }
            } else {
                self.showError(NSLocalizedString(
                    "No Internet connection. \nResignation is unavailable.",
                    comment: ""))
            }
        }
    }

    fileprivate func showError(_ errorString: String) {
        UIAlertView(
            title: NSLocalizedString("Error", comment: ""),
            message: errorString,
            delegate: self,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        self.cancelButtonTapped()
    }

    @objc fileprivate func cancelButtonTapped() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
