//
//  LoginViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import SwiftValidate
import Pantry
import ARSLineProgress

class LoginViewController: FormViewController, LogoPresenter {

    fileprivate let loginService = LoginService()
    fileprivate var validation: ValidatorChain?
    var logoView: UIImageView?

    override func viewDidLoad() {

        typealias progressConf = ARSLineProgressConfiguration
        progressConf.circleColorInner = UIColor.colorWithHexString("87D59E").cgColor
        progressConf.circleColorMiddle = UIColor.colorWithHexString("5BBC7A").cgColor
        progressConf.circleColorOuter = UIColor.colorWithHexString("3AA359").cgColor
        progressConf.checkmarkColor = UIColor.colorWithHexString("5BBC7A").cgColor
        progressConf.successCircleColor = UIColor.colorWithHexString("3AA359").cgColor
        progressConf.failCrossColor = UIColor.colorWithHexString("5BBC7A").cgColor
        progressConf.failCircleColor = UIColor.colorWithHexString("3AA359").cgColor

        validation = setupValidation()
        setupView(validation!)
        self.tryLogin(form.values(), showErrors: false)
        self.setLogoFrame(navigationItem)
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    fileprivate func setupValidation() -> ValidatorChain {
        return ValidatorChain() {
            $0.stopOnFirstError = true
            $0.stopOnException = true
            } <~~ ValidatorRequired() {
                $0.errorMessage = NSLocalizedString("Mandatory field", comment: "")
            }
    }

    fileprivate func setupView(_ validation: ValidatorChain) {
        form +++ Section(" ")

            <<< AccountFloatLabelRow("Login Field") {
                $0.title = NSLocalizedString("Login", comment: "")
                $0.cell.textField.returnKeyType = UIReturnKeyType.done
                $0.cell.textField.enablesReturnKeyAutomatically = true
                $0.cell.textField.becomeFirstResponder()
                if let user: User = Pantry.unpack("lastUser") {
                    $0.value = user.username
                    $0.updateCell()
                }
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textField.isFirstResponder {
                        let validationResults = self.isValidRow(validation,
                            loginRow: row, passwordRow: nil)
                        if !validationResults.valid {
                            self.showError(validationResults.titleString,
                                errorString: validationResults.errorString)
                        }
                    }
                }).cellUpdate({ cell, row in
                    cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                })

            <<< PasswordFloatLabelRow("Password Field") {
                $0.title = NSLocalizedString("Password", comment: "")
                $0.cell.textField.returnKeyType = UIReturnKeyType.done
                $0.cell.textField.enablesReturnKeyAutomatically = true
                if let user: User = Pantry.unpack("lastUser") {
                    $0.value = user.password
                    $0.updateCell()
                }
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textField.isFirstResponder {
                        let validationResults = self.isValidRow(validation,
                            loginRow: nil, passwordRow: row)
                        if !validationResults.valid {
                            self.showError(validationResults.titleString,
                                errorString: validationResults.errorString)
                        }
                    }
                }).cellUpdate({ cell, row in
                    cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                })

            +++ Section("")

            <<< ButtonRow(NSLocalizedString("Login", comment: "")) {
                $0.title = $0.tag
                }.onCellSelection({ (cell, row) in
                    self.tryLogin(self.form.values(includeHidden: false), showErrors: true)
                }).cellUpdate { cell, row in
                    cell.textLabel?.textColor = UIColor.white
                    cell.backgroundColor = UIColor.colorWithHexString("5BBC7A")
                }
    }

    fileprivate func showError(_ rowName: String?, errorString: String) {
        UIAlertView(
            title: rowName != nil ? NSLocalizedString(rowName!, comment: "") : "",
            message: errorString,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    fileprivate func tryLogin(_ values: [String: Any?], showErrors: Bool) {
        if let loginString = values["Login Field"] as? String,
            let passwordString = values["Password Field"] as? String {

            ARSLineProgress.show()
            lockUI(true)

            loginService
                .loginWithCredentials(loginString, passwordString: passwordString) {
                    (user: User?, error) in
                    if let loggedUser = user {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                Pantry.pack(loggedUser, key: "lastUser")
                                Pantry.pack(loggedUser, key: "currentUser")

                                if let isTrainer = loggedUser.isTrainer {
                                    ARSLineProgress.showSuccess()

                                    self.present(
                                        self.drawerController(isTrainer),
                                        animated: true,
                                        completion: {
                                            self.lockUI(false)
                                        }
                                    )
                                } else {
                                    ARSLineProgress.showFail()
                                    self.lockUI(false)
                                }

                        })
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showFail()
                                self.lockUI(false)
                                let errorTitle = NSLocalizedString("Password", comment: "")
                                let errorString = NSLocalizedString("Password is incorrect", comment: "")
                                self.showError(errorTitle, errorString: errorString)
                        })
                    }
                }
        } else {
            if showErrors {
                showError("", errorString: NSLocalizedString("Fill all fields.", comment: ""))
            }
        }
    }

    fileprivate func isValidRow(_ validation: ValidatorChain,
                                     loginRow: AccountFloatLabelRow?,
                                     passwordRow: PasswordFloatLabelRow?) ->
        (valid: Bool, titleString: String, errorString: String) {

        if let row = loginRow {

            let _ = validation.validate(row.value, context: nil)

            if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                let titleString = row.title,
                let errorString = validator?.errors.first {
                return (false, titleString, errorString)
            }
        } else if let row = passwordRow {

            let _ = validation.validate(row.value, context: nil)

            if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                let titleString = row.title,
                let errorString = validator?.errors.first {
                return (false, titleString, errorString)
            }
            
            if let value: String = row.value, value.length < 4,
                let titleString = row.title {
                let errorString = NSLocalizedString("Password is too short", comment: "")
                return (false, titleString, errorString)
            }
        }

        return (true, "", "")
    }

    fileprivate func lockUI(_ lock: Bool) {

        form.allRows.forEach { (b) in
            b.disabled = Condition(booleanLiteral: lock)
            b.hidden = Condition(booleanLiteral: lock)
            b.evaluateDisabled()
            b.evaluateHidden()
        }
    }

    fileprivate func  drawerController(_ isTrainer: Bool) -> UIViewController {

        let trainingListViewController = TrainingListViewController()
        trainingListViewController.adminView = isTrainer
        let navViewController = UINavigationController(
            rootViewController: trainingListViewController)
        navViewController.modalTransitionStyle = .crossDissolve

        let menuViewController = MenuViewController()
        menuViewController.setViewType(isTrainer)
        let menuStack = SSASideMenu(contentViewController:
            UINavigationController(rootViewController: trainingListViewController),
            MenuViewController: menuViewController)

        menuStack.delegate = menuViewController
        menuStack.backImage = UIImage(named: "menu_background.jpg")
        menuStack.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        menuStack.type = .slip
        menuStack.modalTransitionStyle = .crossDissolve
        menuStack.configure(SSASideMenu.MenuViewEffect(
            fade: true,
            scale: true,
            scaleBackground: true))
        menuStack.configure(SSASideMenu.ContentViewShadow(
            enabled: true,
            color: UIColor.black,
            opacity: 0.6,
            radius: 6.0))

        return menuStack
    }
}
