//
//  SignupCredentials.swift
//  BODY SCHOOL
//
//  Created by eltherion on 8/2/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation

class SignupCredentials: JSONAble {

    var userId: String
    var trainingId: String

    init(_ userIdString: String, trainingIdString: String) {
        userId = userIdString
        trainingId = trainingIdString
    }
}
