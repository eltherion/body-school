//
//  AddingSchematicViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import SwiftValidate
import Pantry
import ARSLineProgress

class AddingSchematicViewController: FormViewController, LogoPresenter, UITextViewDelegate {

    fileprivate let schematicService = SchematicService()
    fileprivate var validation: ValidatorChain?
    fileprivate let rowHeight: CGFloat = 150

    var logoView: UIImageView?
    var schematic: Schematic?
    var descr: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setLogoFrame(navigationItem)

        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Cancel", comment: ""),
            style: .plain,
            target: self,
            action: #selector(cancelChanges))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")

        validation = setupValidation()
        setupView(validation!)
    }

    override func viewWillAppear(_ animated: Bool) {

        if let schematicCodeRow = self.form.rowBy(tag: "Schematic code"),
            let schematicNameRow = self.form.rowBy(tag: "Schematic name"),
            let descriptionRow: TextAreaRow = self.form.rowBy(tag: "Description"),
            let minimumUsersCountRow = self.form.rowBy(tag: "Minimum users count"),
            let maximumUsersCountRow = self.form.rowBy(tag: "Maximum users count"),
            let copyCountRow = self.form.rowBy(tag: "Number of copies"){

            schematicCodeRow.baseValue = schematic?.schemaCode
            schematicCodeRow.baseCell.update()

            schematicNameRow.baseValue = schematic?.schemaName
            schematicNameRow.baseCell.update()

            descr = schematic?.descr
            descriptionRow.value = schematic?.descr
            descriptionRow.cell.update()

            minimumUsersCountRow.baseValue = schematic?.minUsersCount
            minimumUsersCountRow.baseCell.update()

            maximumUsersCountRow.baseValue = schematic?.maxUsersCount
            maximumUsersCountRow.baseCell.update()
            
            copyCountRow.baseValue = schematic?.copyCount
            copyCountRow.baseCell.update()
        }

        super.viewWillAppear(animated)
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.validation = nil
        super.viewWillDisappear(animated)
    }

    fileprivate func setupValidation() -> ValidatorChain {
        return ValidatorChain() {
            $0.stopOnFirstError = true
            $0.stopOnException = true
            } <~~ ValidatorRequired() {
                $0.errorMessage = NSLocalizedString("Mandatory field", comment: "")
        }
    }

    func setupView(_ validation: ValidatorChain) {
        form +++ Section()
            <<< self.addAccountFloatLabelRow("Schematic code",
                    title: NSLocalizedString("Schematic code", comment: ""),
                    firstResponder: true)

            <<< self.addAccountFloatLabelRow("Schematic name",
                     title: NSLocalizedString("Schematic name", comment: ""),
                     firstResponder: false)

            <<< LabelRow() {
                $0.title = NSLocalizedString("Description", comment: "")
            }.cellUpdate({
                cell, row in
                cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 12)
                cell.textLabel?.textColor = UIColor.colorWithHexString("5BBC7A")
            })

            <<< TextAreaRow("Description") {
                    $0.textAreaHeight = .dynamic(initialTextViewHeight: self.rowHeight)
                    $0.value = ""
                    $0.cell.update()
                    $0.cell.textView.isEditable = true
                    $0.cell.textView.isSelectable = true
                    $0.cell.textView.delegate = self
                    $0.cell.textView.isScrollEnabled = true
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textView.isFirstResponder {
                        if let validation = self.validation {
                            let validationResults = self.isValidRow(validation,
                                textAreaRow: row)
                            if !validationResults.valid {
                                self.showError(validationResults.titleString,
                                    errorString: validationResults.errorString)
                            }
                        }
                    }
                })

            <<< self.addIntFloatLabelRow("Minimum users count",
                    title: NSLocalizedString("Minimum users count", comment: ""),
                    firstResponder: false)

            <<< self.addIntFloatLabelRow("Maximum users count",
                    title: NSLocalizedString("Maximum users count", comment: ""),
                    firstResponder: false)
            
            <<< self.addIntFloatLabelRow("Number of copies",
                                         title: NSLocalizedString("Number of copies", comment: ""),
                                         firstResponder: false)

            +++ Section()

            <<< SchemeRow().cellSetup({ (cell, row) in
                self.setupSchemeRow(cell)
            })
    }

    fileprivate func addAccountFloatLabelRow(_ tag: String, title: String, firstResponder: Bool) ->
        AccountFloatLabelRow {
        return AccountFloatLabelRow(tag) {
            $0.title = title
            $0.cell.textField.returnKeyType = UIReturnKeyType.done
            $0.cell.textField.enablesReturnKeyAutomatically = true
            if firstResponder {$0.cell.textField.becomeFirstResponder()}
            }.onCellHighlightChanged({ (cell, row) in
                if !cell.textField.isFirstResponder {
                    if let validation = self.validation {
                        let validationResults = self.isValidRow(validation,
                            floatRow: row)
                        if !validationResults.valid {
                            self.showError(validationResults.titleString,
                                errorString: validationResults.errorString)
                        }
                    }
                }
            }).cellUpdate({ cell, row in
                cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
            })
    }

    fileprivate func addIntFloatLabelRow(_ tag: String, title: String, firstResponder: Bool) ->
        IntFloatLabelRow {
            return IntFloatLabelRow(tag) {
                $0.title = title
                $0.cell.textField.returnKeyType = UIReturnKeyType.done
                $0.cell.textField.enablesReturnKeyAutomatically = true
                if firstResponder {$0.cell.textField.becomeFirstResponder()}
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textField.isFirstResponder {
                        if let validation = self.validation {
                            let validationResults = self.isValidRow(validation,
                                floatRow: row)
                            if !validationResults.valid {
                                self.showError(validationResults.titleString,
                                    errorString: validationResults.errorString)
                            }
                        }
                    }
                }).cellUpdate({ cell, row in
                    cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                })
    }

    func textViewDidChange(_ textView: UITextView) {
        descr = textView.text
    }

    fileprivate func showError(_ rowName: String?, errorString: String) {
        UIAlertView(
            title: rowName != nil ? NSLocalizedString(rowName!, comment: "") : "",
            message: errorString,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    fileprivate func isValidRow(_ validation: ValidatorChain,
                            floatRow: AccountFloatLabelRow?) ->
        (valid: Bool, titleString: String, errorString: String) {

            if let row = floatRow {

                let _ = validation.validate(row.value, context: nil)

                if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                    let titleString = row.title,
                    let errorString = validator?.errors.first {
                    return (false, titleString, errorString)
                }
            }
        return(true, "", "")
    }

    fileprivate func isValidRow(_ validation: ValidatorChain,
                            floatRow: IntFloatLabelRow?) ->
        (valid: Bool, titleString: String, errorString: String) {

            if let row = floatRow {

                let _ = validation.validate(row.value, context: nil)

                if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                    let titleString = row.title,
                    let errorString = validator?.errors.first {
                    return (false, titleString, errorString)
                }
            }
            return(true, "", "")
    }

    fileprivate func isValidRow(_ validation: ValidatorChain,
                            textAreaRow: TextAreaRow?) ->
        (valid: Bool, titleString: String, errorString: String) {

            if let row = textAreaRow {

                let _ = validation.validate(row.value, context: nil)

                if let validator: ValidatorRequired? = validation.get(validatorWithIndex: 0),
                    let titleString = row.title,
                    let errorString = validator?.errors.first {
                    return (false, titleString, errorString)
                }
            }
            return(true, "", "")
    }

    fileprivate func lockUI(_ lock: Bool) {

        form.allRows.forEach { (b) in
            b.disabled = Condition(booleanLiteral: lock)
            b.hidden = Condition(booleanLiteral: lock)
            b.evaluateDisabled()
            b.evaluateHidden()
        }
    }

    // MARK: - table/form view delegate methods
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 {
            switch (indexPath as NSIndexPath).row {
            case 2 : return 20
            case 3 : return 150
            default: return 54
            }
        } else {
            return 60
        }
    }

    func tableView(_ tableView: UITableView,
                   shouldHighlightRowAtIndexPath indexPath: IndexPath) -> Bool {
        return (indexPath as NSIndexPath).section == 0
    }

    // MARK: - custom scheme row initialization
    fileprivate func setupSchemeRow(_ cell: UIView) {
        setupRowButtons(cell)
    }

    fileprivate func setupRowButtons(_ cell: UIView) {
        let saveButton = UIButton()
        saveButton.backgroundColor = UIColor.green.withAlphaComponent(0.8)
        saveButton.setTitleColor(UIColor.black, for: UIControlState())
        saveButton.setTitle(NSLocalizedString("Save changes", comment: ""),
                            for: UIControlState())
        saveButton.addTarget(self,
                             action: #selector(saveChanges),
                             for: .touchUpInside)

        let cancelButton = UIButton()
        cancelButton.backgroundColor = UIColor.orange
        cancelButton.setTitleColor(UIColor.black, for: UIControlState())
        cancelButton.titleLabel?.numberOfLines = 0
        cancelButton.titleLabel?.lineBreakMode = .byWordWrapping
        cancelButton.titleLabel?.textAlignment = .center
        cancelButton.setTitle(NSLocalizedString("Cancel without changes", comment: ""),
                              for: UIControlState())
        cancelButton.addTarget(self,
                               action: #selector(cancelChanges),
                               for: .touchUpInside)

        cell.addSubview(saveButton)
        cell.addSubview(cancelButton)

        saveButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.right.equalTo()(cell.mas_centerX)?.offset()(-4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })

        cancelButton.mas_makeConstraints({ (make) in
            let _ = make?.width.equalTo()(140)
            let _ = make?.left.equalTo()(cell.mas_centerX)?.offset()(4)
            let _ = make?.height.equalTo()(46)
            let _ = make?.bottom.equalTo()(cell.mas_bottom)?.offset()(-8)
        })
    }

    @objc fileprivate func saveChanges() {
        self.trySaveSchematic(form.values(), showErrors: true)
    }

    fileprivate func trySaveSchematic(_ values: [String: Any?], showErrors: Bool) {
        if let schematicCode = values["Schematic code"] as? String,
            let schematicName = values["Schematic name"] as? String,
            let description = descr,
            let minimumUsersCount = values["Minimum users count"] as? Int,
            let maximumUsersCount = values["Maximum users count"] as? Int,
            let copyCount = values["Number of copies"] as? Int,
            let user: User = Pantry.unpack("lastUser"),
            let token = user.token {

            ARSLineProgress.show()
            lockUI(true)

            let schematic = Schematic()
            schematic.identifier = self.schematic?.identifier
            schematic.schemaCode = schematicCode
            schematic.schemaName = schematicName
            schematic.descr = String(description)
            schematic.minUsersCount = minimumUsersCount
            schematic.maxUsersCount = maximumUsersCount
            schematic.copyCount = copyCount

            schematicService
                .saveSchematic(schematic, token: token, completion: { (event, error) in
                    if event != nil {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showSuccess()
                                self.lockUI(false)
                                let _ = self.navigationController?.popViewController(animated: true)
                        })
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showFail()
                                self.lockUI(false)
                                self.showError("", errorString: (error?.localizedDescription)!)
                        })
                    }
                })
        } else {
            if showErrors {
                showError("", errorString: NSLocalizedString("Fill all fields.", comment: ""))
            }
        }
    }

    @objc fileprivate func cancelChanges() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
