//
//  SchematicService.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import Pantry

class SchematicService: BaseService {

    fileprivate static func endpoint() -> String { return "/schematic" }
    fileprivate let schematicURLString: String = BaseService.serverURL()
        + SchematicService.endpoint()
    
    fileprivate let schematicFilteredURLString: String = BaseService.serverURL()
        + SchematicService.endpoint()
        + "/filters"
    
    fileprivate let schematicFilterSaveURLString: String = BaseService.serverURL()
        + SchematicService.endpoint()
        + "/save_filters"

    func fetchSchematicListFiltered(_ filtered: Bool, token: String,
                       completion: @escaping (([Schematic]?, NSError?) -> Void)) {
        Alamofire
            .request(filtered ? schematicFilteredURLString : schematicURLString,
                     method: .get,
                     headers: ["X-AUTH-TOKEN": token])
            .responseArray { (response: DataResponse<[Schematic]>)  in
                completion(response.result.value, response.result.error as NSError? as NSError?)
        }
    }

    func saveSchematic(_ schematic: Schematic, token: String,
                 completion: @escaping ((Schematic?, NSError?) -> Void)) {
        Alamofire
            .request(schematicURLString,
                method: .post,
                parameters: schematic.toSavingDictionary(),
                encoding: JSONEncoding.default,
                headers: ["X-AUTH-TOKEN": token])
            .responseObject { (response: DataResponse<Schematic>)  in
                completion(response.result.value, response.result.error as NSError? as NSError?)
        }
    }
    
    func saveSchematicFilters(_ schematicCredentials: [[String:AnyObject]], token: String,
                       completion: @escaping ((NSError?) -> Void)) {
        do {
            var request = URLRequest(url: URL(string: schematicFilterSaveURLString)!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(token, forHTTPHeaderField: "X-AUTH-TOKEN")
            let jsonData = try JSONSerialization.data(withJSONObject: schematicCredentials,
                                                      options: .init(rawValue: 0))
            request.httpBody = jsonData
            
            Alamofire
                .request(request)
                .responseString { (response: DataResponse<String>) in
                    completion(response.result.error as NSError?)
            }
        } catch let error as NSError {
            print(error)
        }
    }


    func deleteSchematicWithIdentifier(_ identifier: Int, token: String,
                                  completion: @escaping ((String?, NSError?) -> Void)) {
        Alamofire
            .request(schematicURLString+"/\(identifier)",
                method: .delete,
                headers: ["X-AUTH-TOKEN": token])
            .responseString { (response: DataResponse<String>)  in
                completion(response.result.value, response.result.error as NSError? as NSError?)
        }
    }
}
