//
//  LoginCredentials.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation

class LoginCredentials: JSONAble {

    var username: String
    var password: String

    init(_ usernameString: String, passwordString: String) {
        username = usernameString
        password = passwordString
    }
}
