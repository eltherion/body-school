//
//  DayColumnHeader.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit

class DayColumnHeader: UICollectionReusableView {

    var day: Date
    var currentDay: Bool
    var holidayDay: Bool
    var title: UILabel
    var titleBackground: UIView
    let dateFormatter: DateFormatter = DateFormatter()

    override init(frame: CGRect) {
        self.day = Date()
        self.currentDay = true
        self.holidayDay = true
        self.title = UILabel()
        self.titleBackground = UIView()
        super.init(frame: frame)
        self.titleBackground.layer.cornerRadius = CGFloat(nearbyintf(15.0))
        self.addSubview(self.titleBackground)
        self.backgroundColor = UIColor.clear
        self.title.backgroundColor = UIColor.clear
        self.addSubview(self.title)
        self.titleBackground.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?
                .edges
                .equalTo()(self.title)!
                .with()
                .insets()(UIEdgeInsets(top: -6.0, left: -12.0, bottom: -4.0, right: -12.0))
        }
        self.title.mas_makeConstraints { (make: MASConstraintMaker?) in
            let _ = make?.center.equalTo()(self)
        }

        dateFormatter.dateFormat = ((UI_USER_INTERFACE_IDIOM() == .phone)
            ? "EEE MMM d"
            : "EEEE MMMM d, YYYY")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func day(_ day: Date) {
        self.day = day

        self.title.text = dateFormatter.string(from: day)
        self.setNeedsLayout()
    }

    func currentDay(_ currentDay: Bool) {
        self.currentDay = currentDay
        if currentDay {
            self.title.textColor = UIColor.white
            self.title.font = UIFont.boldSystemFont(ofSize: 16.0)
            self.titleBackground.backgroundColor = UIColor.colorWithHexString("5BBC7A")
        } else {
            self.title.font = UIFont.systemFont(ofSize: 16.0)
            self.title.textColor = UIColor.black
            self.titleBackground.backgroundColor = UIColor.clear

        }
    }
    
    func holidayDay(_ holidayDay: Bool) {
        self.holidayDay = holidayDay
        if holidayDay {
            self.title.textColor = UIColor.white
            self.title.font = UIFont.boldSystemFont(ofSize: 16.0)
            self.titleBackground.backgroundColor = UIColor.lightGray
        } else if !self.currentDay {
            self.title.font = UIFont.systemFont(ofSize: 16.0)
            self.title.textColor = UIColor.black
            self.titleBackground.backgroundColor = UIColor.clear
            
        }
    }

}
