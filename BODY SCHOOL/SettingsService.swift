//
//  LoginService.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import CryptoSwift
import Alamofire
import AlamofireObjectMapper

class SettingsService: BaseService {

    fileprivate static func endpoint() -> String { return "/setting" }
    fileprivate let settingsURLString: String = BaseService.serverURL()
        + SettingsService.endpoint()

    func fetchSettings(_ token: String,
                       completion: @escaping (([Setting]?, NSError?) -> Void)) {
        Alamofire
            .request(settingsURLString,
                method: .get,
                headers: ["X-AUTH-TOKEN": token])
            .responseArray { (response: DataResponse<[Setting]>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }

    func saveSettings(_ settings: [[String:AnyObject]], token: String,
                                  completion: @escaping ((NSError?) -> Void)) {

        do {
            var request = URLRequest(url: URL(string: settingsURLString)!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(token, forHTTPHeaderField: "X-AUTH-TOKEN")
            let jsonData = try JSONSerialization.data(withJSONObject: settings,
                                                          options: .init(rawValue: 0))
            request.httpBody = jsonData
            
            Alamofire
                .request(request)
                .responseString { (response: DataResponse<String>) in
                    completion(response.result.error as NSError?)
            }
        } catch let error as NSError {
            print(error)
        }
    }
}
