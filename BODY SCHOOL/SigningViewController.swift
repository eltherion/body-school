//
//  SigningViewController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import Pantry
import Timepiece

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

class SigningViewController: FormViewController, LogoPresenter,
    UITextViewDelegate, UIAlertViewDelegate {

    fileprivate let trainingService = TrainingsService()
    fileprivate let settingsService = SettingsService()
    fileprivate let rowHeight: CGFloat = 86
    fileprivate let maxReservationsCount: Int = 4
    fileprivate let trainingSchemeView = EventCell()

    var logoView: UIImageView?
    var training: Training?
    var signedTrainingsCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setLogoFrame(navigationItem)

        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Cancel", comment: ""),
            style: .plain,
            target: self,
            action: #selector(cancelButtonTapped))
        navigationItem.leftBarButtonItem?.tintColor = UIColor.colorWithHexString("5BBC7A")

        setupView()
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    func setupView() {
        let footerString: String
        if let isParticipant = training?.isParticipant , isParticipant {
            footerString = NSLocalizedString("To cancel reservation tap on a specification below.",
                                             comment: "")
        } else {
            footerString = NSLocalizedString("To make reservation tap on a specification below.",
                                             comment: "")
        }
        form +++ Section(
            header: NSLocalizedString("Description:", comment: ""),
            footer: footerString)

            <<< TextAreaRow() {
                if let descr = self.training?.schematic?.descr {
                    $0.cell.textView.insertText(descr)
                }
                $0.textAreaHeight = .dynamic(initialTextViewHeight: self.rowHeight)
                $0.cell.textView.isEditable = false
                $0.cell.textView.isSelectable = false
                $0.cell.textView.delegate = self
            }

            +++ Section()

            <<< SchemeRow(tag: nil).cellSetup({ (cell, row) in
                self.setupSchemeRow(cell)
            })
    }

    // MARK: - text view delegate methods
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return false
    }

    // MARK: - table/form view delegate methods
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath as NSIndexPath).section == 0 {
            return max(rowHeight, self.view.frame.height - rowHeight - 150)
        } else {
            return rowHeight
        }
    }

    func tableView(_ tableView: UITableView,
                   shouldHighlightRowAtIndexPath indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return NSLocalizedString("Description:", comment: "")
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView, section == 0 {
            headerView.textLabel?.text = NSLocalizedString("Description:", comment: "")
        }
    }

    // MARK: - custom scheme row initialization
    fileprivate func setupSchemeRow(_ cell: UIView) {
        if let t = self.training {
            self.trainingSchemeView.training(t)
            cell.addSubview(self.trainingSchemeView)

            self.trainingSchemeView.mas_makeConstraints({ (make) in
                let _ = make?.centerX.equalTo()(cell.mas_centerX)
                let _ = make?.top.equalTo()(cell.mas_top)?.offset()(8)
                let _ = make?.width.equalTo()(254)
                let _ = make?.height.equalTo()(70)
            })

            let gestureRecognizer = UITapGestureRecognizer(
                target: self,
                action: #selector(changeReservation))
            self.trainingSchemeView.addGestureRecognizer(gestureRecognizer)
        }
    }

    @objc fileprivate func changeReservation() {
        if let training = self.training,
            let startDate = training.startDate?.beginningOfDay {
            trainingService.trainingForId(startDate, training: training,
                completion: { (selectedTraining, error) in
                    if let t = selectedTraining {
                        self.saveChanges(t)
                    } else {
                        self.showError(NSLocalizedString(
                            "No Internet connection. \nReservation is unavailable.",
                            comment: ""))
                    }
            })
        }
    }

    fileprivate func saveChanges(_ training: Training) {
        if let user: User = Pantry.unpack("lastUser"),
        let token = user.token,
        let isParticipant = training.isParticipant {
            if !isParticipant {
                self.subscribe(user, training: training)
            } else {
                self.unsubscribe(user, token: token, training: training)
            }
        }
    }

    fileprivate func subscribe(_ user: User, training: Training) {
        if training.startDate < Date() {
            showError(NSLocalizedString(
                "Training has expired. \nReservation is unavailable.",
                comment: ""))
        } else if let isCanceled = training.isCanceled , isCanceled {
            showError(NSLocalizedString(
                "Training was canceled. \nReservation is unavailable.",
                comment: ""))
        } else if let isFull = training.isFull , isFull {
            showError(NSLocalizedString(
                "Training is full. \nReservation is unavailable.",
                comment: ""))
        } else if self.signedTrainingsCount >= maxReservationsCount {
            showError(NSLocalizedString(
                "This week you already have \(maxReservationsCount) reservations. \nReservation is unavailable.",
                comment: ""))
        } else {
            trainingService.signupForTraining(user,
                training: training, completion: { (responseString, error) in
                    if error == nil {
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        self.showError((error?.localizedDescription)!)
                    }
                }
            )
        }
    }

    fileprivate func unsubscribe(_ user: User, token: String, training: Training) {

        settingsService.fetchSettings(token) { (settingsArray, error) in
            if let settings: [Setting] = settingsArray,
            let rtString: String = settings.filter({$0.name == "TERM_OF_APPEAL_PARTICIPATE"})
                                        .first?.value,
            let rt: Int = Int(rtString),
            let startDate = training.startDate {

                if training.startDate < Date() {
                    self.showError(NSLocalizedString(
                        "Training has expired. \nResignation is unavailable.",
                        comment: ""))
                } else if let isCanceled = training.isCanceled , isCanceled {
                    self.showError(NSLocalizedString(
                        "Training was canceled. \nResignation is unavailable.",
                        comment: ""))
                } else if startDate - rt.hours < Date() {
                    self.showError(NSLocalizedString(
                        "Time to resign has expired. \nResignation is unavailable.",
                        comment: ""))
                } else {
                    self.trainingService.unsubscribeFromTraining(user, training: training,
                        completion: { (responseString, error) in
                            if error == nil {
                                _ = self.navigationController?.popViewController(animated: true)
                            } else {
                                self.showError((error?.localizedDescription)!)
                            }
                    })
                }
            } else {
                self.showError(NSLocalizedString(
                    "No Internet connection. \nResignation is unavailable.",
                    comment: ""))
            }
        }
    }

    fileprivate func showError(_ errorString: String) {
        UIAlertView(
            title: NSLocalizedString("Error", comment: ""),
            message: errorString,
            delegate: self,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        self.cancelButtonTapped()
    }

    @objc fileprivate func cancelButtonTapped() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - custom scheme row
open class SchemeCell: Cell<Bool>, CellType {
}

public final class SchemeRow: Row<SchemeCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<SchemeCell>()
    }
}
