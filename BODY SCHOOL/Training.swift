//
//  Training.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/20/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import Timepiece
import ObjectMapper

class Training: Mappable {
    var identifier: Int?
    var startDate: Date?
    var endDate: Date?
    var schematic: Schematic?
    var usersCount: Int?
    var isParticipant: Bool?
    var isFull: Bool?
    var isCanceled: Bool?
    var users: [User]?

    init() {
    }

    init(training: Training) {
        self.identifier = training.identifier
        self.startDate = training.startDate
        self.endDate = training.endDate
        self.schematic = Schematic(schematic: training.schematic)
        self.usersCount = training.usersCount
        self.isParticipant = training.isParticipant
        self.isFull = training.isFull
        self.isCanceled = training.isCanceled
        self.users = training.users
    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        identifier <- map["id"]

        let stringOptional = String(describing: map.JSON["date"])
        let dateString = stringOptional
                            .replacingOccurrences(of: "Optional(", with: "")
                            .replacingOccurrences(of: ")", with: "")
        let intString = dateString
                            .substring(to: dateString.index(dateString.endIndex, offsetBy: -3))
        if let dateInt = Int(intString) {
            let date = Date(timeIntervalSince1970: TimeInterval(dateInt))
            self.startDate = date
            self.endDate = date + 1.hour
        }

        self.isCanceled <- map["canceled"]
        self.schematic <- map["schematic"]
        self.usersCount <- map["currentNumberOfParticipants"]
        self.isParticipant <- map["participant"]
        self.isFull <- map["full"]
    }
    
    func toDictionary() -> [String : AnyObject] {
        var mutableDict: [String: AnyObject] = [:]
        if let schematic = self.schematic { mutableDict["schematic"] = schematic }
        if let startDate = self.startDate {
            let date = Double(UInt64(startDate.timeIntervalSince1970) * UInt64(1000))
            mutableDict["date"] = date as AnyObject?
        }
        return mutableDict
    }
}
