//
//  TimeRowHeader.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/9/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import UIKit

@objc class TimeRowHeader: UICollectionReusableView {

    var title: UILabel
    var time: Date
    let dateFormatter: DateFormatter = DateFormatter()

    override init(frame: CGRect) {
        self.title = UILabel()
        self.time = Date()
        super.init(frame: frame)
        dateFormatter.dateFormat = "HH:mm"
        self.backgroundColor = UIColor.clear
        self.title = UILabel()
        self.title.backgroundColor = UIColor.clear
        self.title.font = UIFont.systemFont(ofSize: 12.0)
        self.addSubview(self.title)
        self.title.mas_makeConstraints({ (make: MASConstraintMaker?) in
            let _ = make?.centerY.equalTo()(self.mas_centerY)
            let _ = make?.right.equalTo()(self.mas_right)?.offset()(-5.0)
        })
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - TimeRowHeader
    func time(_ time: Date) {
        self.time = time
        self.title.text = dateFormatter.string(from: time)
        self.setNeedsLayout()
    }

}
