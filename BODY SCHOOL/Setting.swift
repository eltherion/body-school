//
//  Setting.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/20/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import ObjectMapper

class Setting: Mappable {
    var name: String?
    var value: String?

    init(name: String?, value: String?) {
        self.name = name
        self.value = value
    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        self.name <- map["name"]
        self.value <- map["value"]
    }

    func toDictionary() -> [String : AnyObject] {
        var mutableDict: [String: AnyObject] = [:]
        if let name = self.name { mutableDict["name"] = name as AnyObject? }
        if let value = self.value { mutableDict["value"] = value as AnyObject? }
        return mutableDict
    }
}
