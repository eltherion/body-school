//
//  LoginService.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import CryptoSwift
import Alamofire
import AlamofireObjectMapper

class LoginService: BaseService {

    fileprivate static func endpoint() -> String { return "/user" }
    fileprivate let loginURLString: String =
        BaseService.serverURL()
            + LoginService.endpoint()
            + "/login"
    fileprivate let currentUserURLString: String =
        BaseService.serverURL()
            + LoginService.endpoint()
            + "/current"
    fileprivate let usersListURLString: String =
        BaseService.serverURL()
            + LoginService.endpoint()
    fileprivate let changePasswordURLString: String =
        BaseService.serverURL()
            + LoginService.endpoint()
            + "/change_password"

    func loginWithCredentials(_ loginString: String,
                              passwordString: String,
                              completion: @escaping ((User?, NSError?)) -> Void) {

        let loginCredentials = LoginCredentials(loginString,
                                                passwordString: passwordString.sha256())
        Alamofire
            .request(loginURLString,
                     method: .post,
                     parameters: loginCredentials.toDict(),
                     encoding: JSONEncoding.default)
        .response { (response) in
            if let status = response.response?.statusCode, status == 200 {
                if let token = response.response?.allHeaderFields["X-AUTH-TOKEN"] as? String {
                    self.fetchCurrentUserData(passwordString,
                                              token: token,
                                              completion: completion)
                } else {
                    completion((nil, NSError(domain: "", code: status, userInfo: nil)))
                }
            } else {
                completion((nil, NSError(domain: "", code: 400, userInfo: nil)))
            }
        }
    }

    func fetchCurrentUserData(_ password: String,
                              token: String,
                              completion: @escaping ((User?, NSError?) -> Void)) {
        Alamofire
            .request(currentUserURLString,
                method: .get,
                headers: ["X-AUTH-TOKEN": token])
            .responseObject { (response: DataResponse<User>) in
                if let user: User = response.result.value {
                    user.password = password
                    user.token = token
                    completion(user, response.result.error as NSError? as NSError?)
                }
        }
    }

    func fetchUserList(_ token: String,
                       completion: @escaping (([User]?, NSError?) -> Void)) {
        Alamofire
            .request(usersListURLString,
                method: .get,
                headers: ["X-AUTH-TOKEN": token])
            .responseArray { (response: DataResponse<[User]>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }

    func saveUser(_ user: User, edit: Bool, token: String,
                                  completion: @escaping ((User?, NSError?) -> Void)) {
        Alamofire
            .request(usersListURLString,
                method: edit ? .put : .post,
                parameters: user.toDictionary(edit),
                encoding: JSONEncoding.default,
                headers: ["X-AUTH-TOKEN": token])
            .responseObject { (response: DataResponse<User>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }

    func deleteUserWithIdentifier(_ identifier: Int, token: String,
                       completion: @escaping ((String?, NSError?) -> Void)) {
        Alamofire
            .request(usersListURLString+"/\(identifier)",
                method: .delete,
                headers: ["X-AUTH-TOKEN": token])
            .responseString { (response: DataResponse<String>)  in
                completion(response.result.value, response.result.error as NSError?)
        }
    }
    
    func changePassword(_ changePasswordCredentials: ChangePasswordCredentials, _ token: String,
                 completion: @escaping ((String?, NSError?) -> Void)) {
        Alamofire
            .request(changePasswordURLString,
                     method: .post,
                     parameters: changePasswordCredentials.toDict(),
                     encoding: JSONEncoding.default,
                     headers: ["X-AUTH-TOKEN": token])
            .responseString { (response: DataResponse<String>) in
                completion((response.result.value, response.result.error as NSError?))
        }
    }
}
