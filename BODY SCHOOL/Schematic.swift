//
//  Schematic.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/20/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import Timepiece
import ObjectMapper
import Pantry

class Schematic: Mappable, Storable, JSONAble {
    var identifier: Int?
    var schemaCode: String?
    var schemaName: String?
    var descr: String?
    var minUsersCount: Int?
    var maxUsersCount: Int?
    var copyCount: Int?
    var inFilters: Bool?

    init() {
    }
    
    required init(warehouse: Warehouseable) {
        self.identifier = warehouse.get("id")
        self.schemaCode = warehouse.get("schemaCode")
        self.schemaName = warehouse.get("schemaName")
        self.descr = warehouse.get("descr")
        self.minUsersCount = warehouse.get("minUsersCount")
        self.maxUsersCount = warehouse.get("maxUsersCount")
        self.copyCount = warehouse.get("copyCount")
        self.inFilters = warehouse.get("inFilters")
    }

    init(schematic: Schematic?) {
        self.identifier = schematic?.identifier
        self.schemaCode = schematic?.schemaCode
        self.schemaName = schematic?.schemaName
        self.descr = schematic?.descr
        self.minUsersCount = schematic?.minUsersCount
        self.maxUsersCount = schematic?.maxUsersCount
        self.copyCount = schematic?.copyCount
        self.inFilters = schematic?.inFilters
    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        self.identifier <- map["id"]
        self.schemaCode <- map["schematicCode"]
        self.schemaName <- map["schematicName"]
        self.descr <- map["schematicDescription"]
        self.minUsersCount <- map["minUsersNumber"]
        self.maxUsersCount <- map["maxUsersNumber"]
        self.copyCount <- map["copyNumber"]
        self.inFilters <- map["inFilters"]
    }
    
    func toSavingDictionary() -> [String : AnyObject] {
        var mutableDict: [String: AnyObject] = [:]
        if let identifier = self.identifier { mutableDict["id"] = identifier as AnyObject? }
        if let schemaCode = self.schemaCode { mutableDict["schematicCode"] = schemaCode as AnyObject? }
        if let schemaName = self.schemaName { mutableDict["schematicName"] = schemaName as AnyObject? }
        if let descr = self.descr { mutableDict["schematicDescription"] = descr as AnyObject? }
        if let minUsersCount = self.minUsersCount { mutableDict["minUsersNumber"] = minUsersCount as AnyObject? }
        if let maxUsersCount = self.maxUsersCount { mutableDict["maxUsersNumber"] = maxUsersCount as AnyObject? }
        if let copyCount = self.copyCount { mutableDict["copyNumber"] = copyCount as AnyObject? }
        if let inFilters = self.inFilters { mutableDict["inFilters"] = inFilters as AnyObject? }
        
        return mutableDict
    }
}
