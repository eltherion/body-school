//
//  User.swift
//  BODY SCHOOL
//
//  Created by eltherion on 7/3/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import Foundation
import Pantry
import ObjectMapper

class User: Mappable, Storable {
    var identifier: Int?
    var username: String?
    var password: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var isTrainer: Bool?
    var roles: [String]?
    var token: String?

    init() {
    }

    required init?(map: Map) {

    }

    required init(warehouse: Warehouseable) {
        self.identifier = warehouse.get("id")
        self.username = warehouse.get("username")
        self.password = warehouse.get("password")
        self.firstName = warehouse.get("firstName")
        self.lastName = warehouse.get("lastName")
        self.email = warehouse.get("email")
        self.roles = warehouse.get("roles")
        self.isTrainer = warehouse.get("trainer")
        self.token = warehouse.get("token")
    }

    func mapping(map: Map) {
        identifier <- map["id"]
        username <- map["username"]
        password <- map["password"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        email <- map["email"]
        isTrainer <- map["trainer"]
        roles <- map["roles"]
    }

    func toDictionary(_ edit: Bool) -> [String : AnyObject] {
        var mutableDict: [String: AnyObject] = [:]
        if edit, let identifier = self.identifier { mutableDict["id"] = identifier as AnyObject? }
        if !edit, let username = self.username { mutableDict["username"] = username as AnyObject? }
        if let firstName = self.firstName { mutableDict["firstName"] = firstName as AnyObject? }
        if let lastName = self.lastName { mutableDict["lastName"] = lastName as AnyObject? }
        if let email = self.email { mutableDict["email"] = email as AnyObject? }

        return mutableDict
    }
}
