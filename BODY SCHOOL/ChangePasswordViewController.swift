//
//  ChangePasswordController.swift
//  BODY SCHOOL
//
//  Created by eltherion on 6/29/16.
//  Copyright © 2016 BODY SCHOOL. All rights reserved.
//

import UIKit
import Eureka
import SwiftValidate
import Pantry
import ARSLineProgress

class ChangePasswordViewController: FormViewController, LogoPresenter {

    fileprivate let loginService = LoginService()
    var logoView: UIImageView?
    fileprivate let validationIterator = ValidationIterator() {
        $0.resultForUnknownKeys = true
    }
    var validationOn = true

    override func viewDidLoad() {

        typealias progressConf = ARSLineProgressConfiguration
        progressConf.circleColorInner = UIColor.colorWithHexString("87D59E").cgColor
        progressConf.circleColorMiddle = UIColor.colorWithHexString("5BBC7A").cgColor
        progressConf.circleColorOuter = UIColor.colorWithHexString("3AA359").cgColor
        progressConf.checkmarkColor = UIColor.colorWithHexString("5BBC7A").cgColor
        progressConf.successCircleColor = UIColor.colorWithHexString("3AA359").cgColor
        progressConf.failCrossColor = UIColor.colorWithHexString("5BBC7A").cgColor
        progressConf.failCircleColor = UIColor.colorWithHexString("3AA359").cgColor

        setupValidation()
        setupView()

        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Cancel", comment: ""),
            style: .plain,
            target: self,
            action: #selector(cancelButtonTapped))
        navigationController?.navigationBar.tintColor = UIColor.colorWithHexString("5BBC7A")

        self.setLogoFrame(navigationItem)

        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didRotate(
        from fromInterfaceOrientation: UIInterfaceOrientation) {
        setLogoFrame(navigationItem)
    }

    fileprivate func setupValidation() {
        let user: User? = Pantry.unpack("lastUser")
        
        validationIterator.register(
            chain: ValidatorChain() {
                $0.stopOnFirstError = true
                $0.stopOnException = true
            }
            <~~ ValidatorRequired() {
                $0.errorMessage = NSLocalizedString("Mandatory field", comment: "")
            },
            forKeys: ["Old password", "New password", "New password confirmation"])
        
        validationIterator.register(
            chain: ValidatorChain() {
                $0.stopOnFirstError = true
                $0.stopOnException = true
                }
                <~~ ValidatorEmpty() {
                    $0.allowNil = false
                    $0.errorMessage = NSLocalizedString("Mandatory field", comment: "")
            },
            forKeys: ["Old password", "New password", "New password confirmation"])
        
        validationIterator.register(
            chain: ValidatorChain() {
                    $0.stopOnFirstError = true
                    $0.stopOnException = true
            }
            <~~ ValidatorRegex() {
                    $0.pattern = user?.password
                    $0.errorMessageValueIsNotMatching = NSLocalizedString("Old password text is invalid.", comment: "")
            },
            forKey: "Old password")
        
        validationIterator.register(
            chain: ValidatorChain() {
                $0.stopOnException = true
                $0.stopOnFirstError = true
            }
            <~~ ValidatorStrLen() {
                    $0.minLength = 8
                    $0.maxLength = 30
                    $0.errorMessageTooSmall = NSLocalizedString("Password is too short. Min 8 signs.", comment: "")
                    $0.errorMessageTooLarge = NSLocalizedString("Password is too long. Max 30 signs.", comment: "")
            },
            forKeys: ["New password", "New password confirmation"])
    }

    fileprivate func setupView() {
        form +++ Section(" ")

            <<< PasswordFloatLabelRow("Old password") {
                $0.title = NSLocalizedString("Old password", comment: "")
                $0.cell.textField.returnKeyType = UIReturnKeyType.done
                $0.cell.textField.enablesReturnKeyAutomatically = true
                $0.cell.textField.becomeFirstResponder()
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textField.isFirstResponder {
                        let validationResults = self.isValidRow(oldPasswordRow: row, newPasswordRow: nil, newPasswordConfirmationRow: nil)
                        if !validationResults.valid && self.validationOn {
                            self.showError(validationResults.titleString,
                                errorString: validationResults.errorString)
                        }
                    }
                }).cellUpdate({ cell, row in
                    cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                })

            <<< PasswordFloatLabelRow("New password") {
                $0.title = NSLocalizedString("New password", comment: "")
                $0.cell.textField.returnKeyType = UIReturnKeyType.done
                $0.cell.textField.enablesReturnKeyAutomatically = true
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textField.isFirstResponder {
                        let validationResults = self.isValidRow(oldPasswordRow: nil, newPasswordRow: row, newPasswordConfirmationRow: nil)
                        if !validationResults.valid && self.validationOn {
                            self.showError(validationResults.titleString,
                                errorString: validationResults.errorString)
                        }
                    }
                }).cellUpdate({ cell, row in
                    cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                })

            <<< PasswordFloatLabelRow("New password confirmation") {
                $0.title = NSLocalizedString("New password confirmation", comment: "")
                $0.cell.textField.returnKeyType = UIReturnKeyType.done
                $0.cell.textField.enablesReturnKeyAutomatically = true
                }.onCellHighlightChanged({ (cell, row) in
                    if !cell.textField.isFirstResponder {
                        let validationResults = self.isValidRow(oldPasswordRow: nil, newPasswordRow: nil, newPasswordConfirmationRow: row)
                        if !validationResults.valid && self.validationOn {
                            self.showError(validationResults.titleString,
                                           errorString: validationResults.errorString)
                        }
                    }
                }).cellUpdate({ cell, row in
                    cell.floatLabelTextField.titleTextColour = UIColor.colorWithHexString("5BBC7A")
                })

            +++ Section("")

            <<< ButtonRow(NSLocalizedString("Change password", comment: "")) {
                $0.title = $0.tag
                }.onCellSelection({ (cell, row) in
                    self.tryChangePassword(self.form.values(includeHidden: false), showErrors: true)
                }).cellUpdate { cell, row in
                    cell.textLabel?.textColor = UIColor.white
                    cell.backgroundColor = UIColor.colorWithHexString("5BBC7A")
                }
    }

    fileprivate func showError(_ rowName: String?, errorString: String) {
        UIAlertView(
            title: rowName != nil ? NSLocalizedString(rowName!, comment: "") : "",
            message: errorString,
            delegate: nil,
            cancelButtonTitle: NSLocalizedString("OK", comment: ""))
            .show()
    }

    fileprivate func tryChangePassword(_ values: [String: Any?], showErrors: Bool) {
        if let oldPasswordString = values["Old password"] as? String,
            let newPasswordString = values["New password"] as? String,
            let newPasswordConfirmationString = values["New password confirmation"] as? String {
            
            if oldPasswordString == newPasswordString {
                showError("", errorString: NSLocalizedString("Old and new passwords can not be the same.", comment: ""))
            } else if newPasswordString != newPasswordConfirmationString {
                showError("", errorString: NSLocalizedString("Passwords must be the same.", comment: ""))
            } else if let user: User = Pantry.unpack("lastUser"),
                let token = user.token {
                ARSLineProgress.show()
                lockUI(true)
                
                let changePasswordCredentials = ChangePasswordCredentials(newPasswordString, newPasswordConfirmationString)
                
                loginService
                    .changePassword(changePasswordCredentials, token) {
                        (responseString, error) in
                        if error == nil {
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in

                                ARSLineProgress.showSuccess()
                                user.password = newPasswordString
                                Pantry.pack(user, key: "lastUser")
                                Pantry.pack(user, key: "currentUser")
                                
                                self.cancelButtonTapped()
                            })
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                () -> Void in
                                ARSLineProgress.showFail()
                                self.lockUI(false)
                                
                            })
                        }
                }
            }

        } else if showErrors{
                showError("", errorString: NSLocalizedString("Fill all fields.", comment: ""))
        }
    }

    fileprivate func isValidRow(oldPasswordRow: PasswordFloatLabelRow?,
                                newPasswordRow: PasswordFloatLabelRow?,
                                newPasswordConfirmationRow: PasswordFloatLabelRow?) ->
        (valid: Bool, titleString: String, errorString: String) {
            
            let formResults = self.form.values()
            let validationResult = validationIterator.validate(formResults)
            
            if let error = validationIterator.getAllErrors().first,
                let errorString = error.value.first {
                let titleString = error.key
                return (validationResult, titleString, errorString)
            } else {
                return (validationResult, "", "")
            }
    }

    fileprivate func lockUI(_ lock: Bool) {

        form.allRows.forEach { (b) in
            b.disabled = Condition(booleanLiteral: lock)
            b.hidden = Condition(booleanLiteral: lock)
            b.evaluateDisabled()
            b.evaluateHidden()
        }
    }

    @objc fileprivate func cancelButtonTapped() {
        validationOn = false
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
